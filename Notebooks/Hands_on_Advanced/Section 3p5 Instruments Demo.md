---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.6
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Polarization Resolved Mueller SOP Alignment Demo (supplementary material)
![demo](Keysight_OFC_demo2.png)
+++

## Create Resource Manager Object

```{code-cell} ipython3
import time
import numpy as np
import matplotlib.pyplot as plt
import visa
rm = visa.ResourceManager()
```

## Connect to Laser Source

```{code-cell} ipython3
## Open Connection
tls = rm.open_resource('GPIB0::20::INSTR')
tls.timeout = 10000
## Reset Laser
tls.write('*RST')
complete = tls.query('*OPC?')
## Query ID
tls.query('*IDN?')
```

## Connect to Polarization Synthesizer

```{code-cell} ipython3
## Open Connection
polsyn = rm.open_resource('GPIB0::10::INSTR')
polsyn.timeout = 10000
## Reset Power Meter
polsyn.write('*RST')
complete = polsyn.query('*OPC?')
##Query ID
polsyn.query('*IDN?')
```

## Connect to Power Meter

```{code-cell} ipython3
## Open Connection
mppm = rm.open_resource('GPIB0::10::INSTR')
mppm.timeout = 10000
## Reset Power Meter
mppm.write('*RST')
complete = mppm.query('*OPC?')
##Query ID
mppm.query('*IDN?')
```

## Setup Laser Source

```{code-cell} ipython3
tls_wave = 1550e-9
tls_power = -10
tls_mod_freq = 100e-3
## Setup Wavelength
tls.write('SOUR0:WAVE ' + str(tls_wave))
## Setup Power
tls.write('SOUR0:POW:UNIT 0')
tls.write('SOUR0:POW '+str(tls_power))
## Turn On Output Power
tls.write('OUTP0:STAT 1')
```

## Setup Power Meter

```{code-cell} ipython3
mppm_wave = '1550e-9'
mppm_avg = '1000ms'
## Setup Wavelength
mppm.write('SENS1:POW:WAV ' + mppm_wave)
## Setup Averaging Time
mppm.write('SENS1:POW:ATIME ' + mppm_avg)
## Manual Range Mode
mppm.write('SENS1:POW:RANGE:AUTO 0')
mppm.write('SENS1:POW:GAIN:AUTO 0')
## Set Range
mppm.write('SENS1:POW:RANG 10DBM')
## Change Units to Watt
mppm.write('SENS1:POW:UNIT 1')
```

## Setup Polarization Synthesizer

```{code-cell} ipython3
## Stop Polarization Controller
polsyn.write('POL:STOP')
## Stop Polarization Synthesizer
polsyn.write('PCON:STOP')
time.sleep(1)
complete = polsyn.query('*OPC?')
## Delay
polsyn.write("PCON:SEQ:HOLD 1")
## Activate Polarization Stabilization
polsyn.write("STAB:STAB 1")
time.sleep(1)
```

## Stabilize Polarization and Measure Power - 4x Power Measurements

```{code-cell} ipython3
## Define Stokes Vector for LH, LV, L45, RHC Polarization
sop_array = np.array([[1,0,0],[-1,0,0],[0,1,0],[0,0,1]])
power_array = np.zeros(4)
for i in range(0,4):
    sop = sop_array[i]
    polsyn.write('STAB:SOP %s,%s,%s'%(str(SOP[0]),str(SOP[1]),str(SOP[2]))
    complete = polsyn.query('*OPC?')
    pow[i] = float(mppm.query('READ1:POW?'))
    
```

## Calculate Stokes Vector for Min/Max Loss

+++
See Application Note - [Polarization Resolved Mueller](https://www.keysight.com/us/en/assets/7018-01231/application-notes/5989-1261.pdf)

```{code-cell} ipython3
m11 = (1/2)*(pow[0]+pow[1])
m12 = (1/2)*(pow[0]-pow[1])
m13 = pow[2]-m11
m14 = pow[3]-m11
S0 = np.sqrt(m12**2+m13**2+m14**2)
SOP_min=np.array([m12/S0,m13/S0,m14/S0])
SOP_max=np.array([-m12/S0,-m13/S0,-m14/S0])
```

## Stabilize State of Polarization (SOP) for Min Loss & Query Power

```{code-cell} ipython3
polsyn.write('STAB:SOP %s,%s,%s'%(str(SOP_min[0]),str(SOP_min[1]),str(SOP_min[2]))
complete = polsyn.query('*OPC?')
pow_min = mppm.query('READ1:POW?')
```

## Stabilize State of Polarization (SOP) for Max Loss & Query Power

```{code-cell} ipython3
polsyn.write('STAB:SOP %s,%s,%s'%(str(SOP_max[0]),str(SOP_max[1]),str(SOP_max[2]))
complete = polsyn.query('*OPC?')
pow_max = mppm.query('READ1:POW?')
```

## Close Instruments

```{code-cell} ipython3
tls.close()
polsyn.close()
mppm.close()
```
