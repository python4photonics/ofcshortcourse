---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Writing automated tests with pytest

In this section we will go over writing automated tests using pytest which is a testing framework for python. 

## Learning outcomes

You will learn about
* Why you should write automated test
* How to write and run easy tests for pytest'
* Use numpy testing for 
* How to write multi-parameter tests


## Required

* pytest module

## Further reading
* [coderefinery automated testing](https://coderefinery.github.io/testing/)
* [Automation testing with pytest](https://medium.com/tenable-techblog/automation-testing-with-pytest-444c8b34ead2)
* [pytest documentation](https://docs.pytest.org/en/latest/)

### Acknowledgements
* Some of the material in these notes has been adapted from [coderefinery automated testing](https://coderefinery.github.io/testing/) licenced under [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/)

+++

## Why use automated testing

As hardware engineers we are often rightfully concerned with accuracy, reproducability and calibration of our measurement equipment and when we test equipment we often compare against an expected response, performance ... However, often we take much less care about our software and numerical code and if it performs correctly and instead do basic ad-hoc testing. For example do you always confirm that a change you made to your code did not result in an unexpected side-effect?

Two horror stories:
* [A Scientist’s Nightmare: Software Problem Leads to Five Retractions](https://science.sciencemag.org/content/314/5807/1856.summary)
* [Researchers find bug in Python script may have affected hundreds of studies](https://arstechnica.com/information-technology/2019/10/chemists-discover-cross-platform-python-scripts-not-so-cross-platform/)

Automatic testing allows you to:
* ensure that expected functionality is preserved
* verify that code is doing what it is supposed to do
* easier refactoring of code
* easier contributions from external developers

+++

## Testing concepts

Imperfect tests that exists and are run are better than perfect tests that do not exist. 

When testing you should
* test often
* ideally test automatically (using continuous integration for example)
* test with numerical accuracy in mind

There are basically three types of tests. 

### Unit tests

* Unit tests test a single unit, e.g. module or function
* Provide documentation of capability of the function (some frameworks even integrate them into the documentation)

### Integration tests

Also called functionality tests 
* Verify that your modules are working well together
* Test the functionality of your project, e.g. do your simulations get correct results for known cases

### Regression tests

* Test over different versions of the code base
* Can for example test if performance remains the same or does not regress

We will not discuss regression tests here and largely focus on unit tests. 

### Test frameworks

There exists a number of testing frameworks for Python, many with different advantages and disadvanteges. The most common are:
* [pytest](http://doc.pytest.org/)
* [nose](http://nose.readthedocs.io/)
* [doctest](https://docs.python.org/2/library/doctest.html)
* [unittest](https://docs.python.org/2/library/unittest.html)

We will focus on pytest. 

+++

## pytest

Pytest is easy to use and write test for. 

### Installation 

If you are using _anaconda_ you can install pytest via `conda` or the _anaconda navigator_. It is also contained in many linux distributions or alternatively it can be installed with `pip install pytest`. 

+++

## A first test

Let us write a function an test it's functionality. Here we are writing function and tests in the same file, in practical projects tests are typically stored in a separate directory (e.g. tests) and the functions and modules to be tested are imported. 

```python
# contents of test_example1.py
def fahrenheit2celcius(T_f):
    """
    Convert temperature in Fahrenheit to Celcius
    """
    T_c = (T_f - 32.) * (5/9.)
    return T_c


def test_fahrenheit2celcius():
    T_c = fahrenheit2celcius(32.)
    expected_result = 0.
    assert T_c == expected_result
```

```{code-cell} ipython3
!pytest test_example1.py
```

Above we have a function which converts temperature in Fahrenheit to temperature in Celcius. We test that the function works correctly by testing against a known result at 0 degress Celcius. 

_pytest_ will run all files of the form `test_*.py` or `*_test.py` in the current directory and 
* all `test` prefixed functions or methods inside these files 
* all `test` prefixed functions or methods inside `Test` prefixed test classes (without an __init__ method)

You can review the test discovery conventions [here](http://doc.pytest.org/en/latest/goodpractices.html#test-discovery)

Generally you should use assert in your tests as this will allow pytest to use advanced assertion introspection which will give you more information on the test failure. 

## Parametrizing

Now in the above test we only test for a single result. We could write test for multiple known results, but that quickly becomes tedious. Fortunately pytest offers a way to avoid much of this boilerplate. 

```python
import pytest

# contents of test_example2.py
def fahrenheit2celcius(T_f):
    """
    Convert temperature in Fahrenheit to Celcius
    """
    T_c = (T_f - 32.) * (5/9.)
    return T_c


@pytest.mark.parametrize("t", [(32,0), (451, 232.778)])
def test_fahrenheit2celcius(t):
    T_c = fahrenheit2celcius(t[0])
    expected_result = t[1]
    assert T_c == expected_result
```


```{code-cell} ipython3
!pytest test_example2.py
```

This test iterated over all parameters, however it fails for the second parameter set due to numerical accuracies. 

### Numpy testing

We could use introduce our own way of accounting for numerical accuracy and do somthing like:
```python
assert abs(T_c - expected_result) < 1e-5
```
Fortunately there is the numpy testing module that was specifically designed for testing numerical code with limited accuracty, so lets use that instead.
```python
import pytest
import numpy.testing as npt

# contents of test_example3.py
def fahrenheit2celcius(T_f):
    """
    Convert temperature in Fahrenheit to Celcius
    """
    T_c = (T_f - 32.) * (5/9.)
    return T_c


@pytest.mark.parametrize("t", [(32,0), (451, 232.778)])
def test_fahrenheit2celcius(t):
    T_c = fahrenheit2celcius(t[0])
    expected_result = t[1]
    npt.assert_almost_equal(T_c, expected_result, 3)
```

```{code-cell} ipython3
!pytest test_example3.py
```

```{code-cell} ipython3
import numpy.testing as npt
help(npt.assert_almost_equal)
```

`numpy.testing` also contains test for equality of arrays testing for absolute or relative equality etc. We highly recommend to use it in your tests.

## Testing for exceptions

Sometimes it might be desirable to test that functions raise exceptions when given the wrong input, instead of continuing and possibly producing bogus results much further down the line (which is often much harder to debug). 

In our example it does not make sense that the input temperature is complex, but the conversion function would still calculate with a complex input which might have unexpected consequences further down the line. Let us raise and exception and test for it.

```python
!cat test_example4.py

# contents of test_example4.py
import pytest
import numpy.testing as npt
import numpy as np

def fahrenheit2celcius(T_f):
    """
    Convert temperature in Fahrenheit to Celcius
    """
    if not np.isreal(T_f):
        raise TypeError("Temperature needs to be a real value")
    T_c = (T_f - 32.) * (5/9.)
    return T_c


@pytest.mark.parametrize("t", [(32,0), (451, 232.778)])
def test_fahrenheit2celcius(t):
    T_c = fahrenheit2celcius(t[0])
    expected_result = t[1]
    npt.assert_almost_equal(T_c, expected_result, 3)

def test_fahrenheit2celcius_type():
    with pytest.raises(TypeError):
        fahrenheit2celcius(1+1j)
```

```{code-cell} ipython3
!pytest test_example4.py
```

## Grouping tests

Sometimes it is desirable to group tests that belong together. For example group all tests for a specific function. This can be done by placing all the tests into a class prefixed with `Test`. Here is an example:

```python
# contents of test_example5.py
import pytest
import numpy.testing as npt
import numpy as np

def celcius2fahrenheit(T_c):
    """
    Convert temperature in Fahrenheit to Celcius
    """
    if not np.isreal(T_c):
        raise TypeError("Temperature needs to be a real value")
    T_f = 9/5* T_c  + 32.
    return T_f

def fahrenheit2celcius(T_f):
    """
    Convert temperature in Fahrenheit to Celcius
    """
    if not np.isreal(T_f):
        raise TypeError("Temperature needs to be a real value")
    T_c = (T_f - 32.) * (5/9.)
    return T_c

class TestConversionf2c(object):
    @pytest.mark.parametrize("t", [(32,0), (451, 232.778)])
    def test_value(self, t):
        T_c = fahrenheit2celcius(t[0])
        expected_result = t[1]
        npt.assert_almost_equal(T_c, expected_result, 3)

    def test_type():
        with pytest.raises(TypeError):
            fahrenheit2celcius(1+1j)


class TestConversionc2f(object):
    @pytest.mark.parametrize("t", [(0, 32), ( 232.778, 451)])
    def test_value(self, t):
        T_c = fahrenheit2celcius(t[0])
        expected_result = t[1]
        npt.assert_almost_equal(T_c, expected_result, 3)

    def test_type():
        with pytest.raises(TypeError):
            fahrenheit2celcius(1+1j)
```

```{code-cell} ipython3
!pytest test_example5.py
```

### Specifying tests

This has the advantage that we can run the tests of only one group (class), for example if we working on one function but don't want to rerun all tests in the file (e.g. because it takes to long). To run only the test in the Celcius to Fahrenheit conversion tests one would use:

```{code-cell} ipython3
!pytest test_example5.py::TestConversionc2f
```

One can similarly only run the test of one method

```{code-cell} ipython3
!pytest test_example5.py::TestConversionc2f::test_type
```

More complex selections are also possible (see [pytest docs specifying test](https://docs.pytest.org/en/latest/usage.html#specifying-tests-selecting-tests) for more details). For example run all type tests.

```{code-cell} ipython3
!pytest test_example5.py -k "test_type"
```

## Integration for automated testing

We do not have the time to cover how to integrate testing with other systems to achieve fully automated testing, however it should be noted that testing develops its full power when it integrates with the build or version control system to create a fully automated test environment. Two possibilities are:

* integrate testing with the build system (setuptools, disttools) to run tests every time a new release is made.
* integrate with the hosted version control system to run tests every time changes are added to the master (or a release branch). This is typically achieved using so-called _continuous integration_. 

+++

## Note on hardware

While we focused here on running tests for software packages, it is quite easy to extend this work with hardware. This would enable to straight forwardly test performance of measurements or devices. [Luceda Photonics](https://www.lucedaphotonics.com/en) have shown some interesting work on automated testing of integrated circuits from design and modelling to measurement and validation of fabricated devices. 

+++

## Summary

Hopefully we have given you a taste of how you can use automated testing to improve your wor
