---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# An Introduction to PyVISA

Interfacing to measurement instruments can be a pain. There exist many protocols, interfaces send over many different bus systems. The  Virtual Instrument Software Architecture (VISA) specification was defined in the middle of the 90ies to standardize the different protocols and allow interfacing different devices over different bus systems with a single library, Thus avoiding having to find the correct library for every device in an experiment.

## Summary

We will briefly revisit some of the basics about pyVISA covered in SC469.


## Requirements

You need:
* PyVISA
* PyVISA-py (a Python implementation of the VISA standard)
* (optional) NI-VISA or Agilent instrumentation 
* (optional) PyVISA-sim

+++

## Installation

PyVISA can be installed using the conda 
```conda install pyvisa```
or pip 
```pip install pyvisa```

It also needs an installed backend. It contains a wrapper for NI-VISA or a compatible implementation by default, but we will also be using PyVISA-py a python implementation of the VISA standard, you can install it with:
`conda install pyvisa-py` or `pip install pyvisa-py`. The simulation backend can be installed with `pip install pyvisa-sim`

Test your installation:

```{code-cell} ipython3
import pyvisa as visa # note the recommended way to import pyvisa changed as there is an unrelated visa module
import numpy as np
rm = visa.ResourceManager()
print(rm.list_resources())
```

## Further Reading

* [PyVISA documentation](https://pyvisa.readthedocs.io/)

+++

## Configuring the backend

The backend is chosen when initiating the backend. By default PyVISA will use the NI-VISA backend. Therefore the code

```{code-cell} ipython3
rm = visa.ResourceManager()
```

Will open the interface to the NI-VISA library. In general PyVISA is very good at finding the location of the library, however it is possible to also specify the path if the library is located in an odd location `rm = visa.ResourceManger("/path/to/library.dll")`. It is also possible to specify the path in a configuration file (see the [PyVISA documentation](https://pyvisa.readthedocs.io/en/master/index.htm) for more details and troubleshooting)

+++

### Backends

As far as we are aware Pyvisa works with all backends. The two most common and functionally complete ones are
* the National Instruments VISA backend (NI-VISA) 
* Keysights IO libraries suite. 

There are also:
* An open source Python backend
* A simulation backend

+++

### Python backend

Installation of the NI-VISA library (or a compatible implementation) can sometimes be painful. Particular on linux and embedded computers. Instead you can use the python implementation. Be aware that it is not as feature complete as the NI-VISA implementation, in particular drivers to certain GPIB devices (e.g. some GPIB/Ethernet bridges) are missing.

To use the Python backend (if installed) use:

```{code-cell} ipython3
rm = visa.ResourceManager('@py')
```

## Simulation backend

The simulation backend (pyvisa-sim) is useful for writing drivers to simulated devices if the instrument is not available. It's very good for testing for example. Simulation backends and devices are defined in YAML files. See the the [PyVISA-sim documentation](https://pyvisa-sim.readthedocs.io/en/latest/index.html) for more information.

To use the pyvisa-sim backend create the resource manager with:

```{code-cell} ipython3
rm = visa.ResourceManager('@sim')
```

To use a specific simulation file start with:

```{code-cell} ipython3
rm = visa.ResourceManager("thorlabs.yml@sim")
```

where `thorlabs.yml` is the YAML specification file.

+++

To find out what backend we are using we can use the `visalib.library_path` attribute of the resource manager object

```{code-cell} ipython3
print(rm.visalib.library_path)
```

## How to use PyVISA

PyVISA is an object oriented "pythonic" interface to the VISA library. The two central objects are the Resource manager and Resources.

### Resources

PyVISA resources are essentially the devices (instruments) connected to the different bus systems (GPIB, USB ...). 

### Resource Manger

The Resource Manager is responsible for listing available device, opening new devices and deciding what resource type a new device should have.

+++

### Basic Example

Let's look at a basic example which lists several resources and opens the first available resource.

```{code-cell} ipython3
rm = visa.ResourceManager('thorlabs.yml@sim')
l = rm.list_resources()
print(l)
inst1 = rm.open_resource(l[0])
```

```{code-cell} ipython3
print(inst1)
```

This is a device on the first serial port.

+++

Alternatively it is possible to open resources simply by specifying the instrument (see the [PyVISA documentation](https://pyvisa.readthedocs.io/en/master/names.html) for the syntax for resource names). The following code opens the same device on serial port 1

```{code-cell} ipython3
inst2 = rm.open_resource("ASRL1::INSTR",read_termination = '\n', write_termination="\r\n")
```

```{code-cell} ipython3
inst2.query("?IDN")
```

This is a dummy instrument that is created by default by pyvisa-sim. 

The following opens up a simulated Thorlabs PM100 optical USB powermeter.

```{code-cell} ipython3
inst = rm.open_resource("USB0::0x1111::0x2222::0x1234::0::INSTR", read_termination="\n", write_termination="\n")
```

## How to read and write to instruments

PyVISA resources provide three basic methods for writing and reading (as well as some more advanced methods to be covered in the later examples).
* `inst.write()` is for sending a command to an instrument (e.g. a setting)
* `inst.read()` is to read a value from the instrument (e.g. a measurement)
* `inst.query()` is a convenient combination of write and read (e.g. to ask for a value and read the value).

The following example uses the SCPI command to ask the connected powermeter for its identifier string. Once using `write` and `read` and once using `query`

```{code-cell} ipython3
inst.write("*IDN?")
id1 = inst.read()
id2 = inst.query("*IDN?")
print(id1)
print(id2)
```

## Configuration of communication parameters

Sometimes it is necessary to configure the communication with certain instruments. Two of the most common parameters are:
* timeout -- the time in ms to wait for a response
* termination character -- the character used for indicating the end of a message

These parameters can either be set when opening a resource or by assigning to the attribute

```{code-cell} ipython3
inst3 = rm.open_resource(l[0], timeout=1000, read_termination='\r')
inst3.timeout = 2000
```

### Return Values

It's important to note that PyVISA by default does not take care of converting your return values into e.g. numpy arrays for you. Most instruments return values in ASCII format, so conversion is typically straight forward. 

```{code-cell} ipython3
# code would look something like this
ret = inst.query("FETCH?")
print(ret)
print(type(ret))
x = float(ret)
print(x)
```

#### Convenience functions

If your instrument returns a list of numbers like a OSA trace measurement. Pyvisa offers some convenience functions which can do the conversion automatically for you. For ASCII values `instrument.query_ascii_values` returns a list of values (in ascii format). 

```{code-cell} ipython3
inst2 = rm.open_resource("TCPIP0::localhost:3333::inst0::INSTR", read_termination="\n", write_termination="\n")
values = inst2.query_ascii_values("MEAS?")
val1 = inst2.query("MEAS?")
print(type(val1))
print(values)
print(type(values))
print(type(values[0]))
varray = np.array(values) # converts the values into an array
```

If you get a large amount of data you might want to avoid the intermediate list by using the `container` keyword argument.

```{code-cell} ipython3
varray =  inst2.query_ascii_values("MEAS?", container=np.array)
print(varray)
print(type(varray))
```

There are also other the `converter` and `separator` arguments which allow more flexible conversions. See the [pyvisa documentation](https://pyvisa.readthedocs.io/en/master/rvalues.html) for details.

+++

## Binary transfer

Most instruments can transfer data in two formats ASCII and binary. There are advantages and disadvantages to both.

ASCII: human readable, easy to debug, but transfer is much slower

Binary: much faster transfers, more difficult to debug because not easily readable

**Tip:** We found that a good approach is to use ASCII transfer for most cases (in particular when writing new instruments) and only switch to binary format for instruments that transfer a lot of data, like oscilloscopes, spectrum analysers ...

PyVISA does also provide functions to easy conversions from binary formats. The naming convention follows the same as the [struct module](https://docs.python.org/3/library/struct.html#format-characters).

```{code-cell} ipython3
values = inst2.query_binary_values('BIN?', datatype='d', is_big_endian=False) # this code does not work here
```

PyVISA assumes by default that your instrument follows `IEEE` conventions, but you can pass different header formats with the `header_fmt` parameter. 

**Important** Not all instruments add a termination character when sending binary data, which results in errors or truncated blocks because PyVISA expects a termination character by default. Please see the [documentation](https://pyvisa.readthedocs.io/en/latest/introduction/rvalues.html#reading-binary-values) when it is save to pass the `expect_termination=False` parameter.

+++

## Further Reading

* [PyVISA documentation](https://pyvisa.readthedocs.io/)

+++

## Exercises

* Download the thorlabs.yml file and use it as a simulated backend
* try setting the averaging of the powermeter (look at the manual at https://www.thorlabs.com/thorproduct.cfm?partnumber=PM100USB)
* try to change the measurement (and check that it works)

```{code-cell} ipython3

```
