---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Final Words


Thank you everyone for participating!

+++

## Reminder

* Feel free to email us further questions.
* Our emails: [nicolas.fontaine@nokia-bell-labs.com](mailto:nicolas.fontaine@nokia-bell-labs.com), [Binbin.Guan@microsoft.com](mailto:Binbin.Guan@microsoft.com), [jochen.schroeder@chalmers.se](mailto:jochen.schroeder@chalmers.se)
* Remember you can find all the material in our git repository at https://gitlab.com/python4photonics/ofcshortcourse
* Also we are running Labautomation Hackathons at OFC, ECOC, Photonics West (and possibly more)
* Visit [python4photonics.org](https:///www.python4photonics.org) where we will post news etc. (let us know if you want to post content)

```{code-cell} ipython3

```
