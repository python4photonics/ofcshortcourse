//"C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvarsx86_amd64.bat" && set OMP_NUM_THREADS=3 && cl /LD /Ox /openmp iii.c
#include <stdio.h>
//MMX
//#include <mmintrin.h>
//SSE
#include <xmmintrin.h>
//SSE2
#include <emmintrin.h>
//SSE3
#include <pmmintrin.h>
//SSE4
#include <smmintrin.h>
//AVX
#include <immintrin.h>
#include <math.h>

// Stuff 
#ifdef __cplusplus
#define EXT_C extern "C"
#else
#define EXT_C
#endif

//Proper DLL expert for linux or windows
#ifdef _WIN32
	#define EXPORT EXT_C __declspec(dllexport)
#else
	#define EXPORT EXT_C __attribute__((visibility("default")))
#endif

// Simple function to export
EXPORT int sum(int a, int b) {
    return a + b;
}

// more complicated function to export
EXPORT void dum(float *a) {
    a[0]=1;
}

// Extremely coplicated function to export that takes in float32 arrays represented as vectors
EXPORT void broadcastrealcomplexmul(__m128 *realarray,float *mode,__m128 *out,unsigned int M, unsigned int N)
{

	const int blockSize = 4;
	const int n = N; // 1 complexes at a time
	const int mr = M/ (4); // 4 reals step
	const int mc = M/ (2); //compl step
  
	//real array has shape N,M/4
	//out array has shape  N,M/2
	//mode has shape of N 
	//go the wrong way through the array??
	//load 4 reals to be multiplied by complex?
		for (int i = 0; i < n; i ++)
		{
            //i is the row we are at... do 2 complex rows at a time....

			//Extract real and imaginary components for 4 numbers
			
			__m128 yR = _mm_set1_ps(mode[2*i]);//_mm_shuffle_ps(y0, y1, _MM_SHUFFLE(0, 2, 0, 2));
			__m128 yI = _mm_set1_ps(mode[2*i+1]);//_mm_shuffle_ps(y0, y1, _MM_SHUFFLE(1, 3, 1, 3));
			
			for (int k=0; k<mr ; k++)
			{

                
                
				//load 4 numbers from real array...
				__m128 rv = realarray[k+i*mr]; // skip m for next row
				
				__m128 xR = _mm_mul_ps(rv,yR); //4 real multiplies
				__m128 xI = _mm_mul_ps(rv,yI); //4 imag multiplies
			
				__m128 lo = _mm_unpacklo_ps(xR, xI);
				__m128 hi = _mm_unpackhi_ps(xR, xI);

				out[    i*mc  + 2*k   ] =  lo;
				out[    (i)*mc  + 2*k+1 ] = hi;
			

			}
				
		}

}
