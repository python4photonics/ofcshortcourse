---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# A basic introduction to Python

While it is not possible to give ab exhaustive introduction to Python in the short time of this course, we aim to give a brief introduction into the main features that are important for the rest of the course. If you have never used Python before, we recommend you to familiarize yourself with it before the course, by using one of the many excellent resources on the net, like the official [Python Tutorial](https://docs.python.org/3/tutorial/)

## Learning Outcomes

* A basic understanding of the Python programming language
* Learn how to install Python using Miniforge
* Learn about basic types and their usage
* Learn how to write and use functions and classes
* What are modules and how to use them
* Why you should almost always use Numpy

+++

## Why Python

### [Trend based on Github users](https://www.developer-tech.com/news/2019/nov/08/octoverse-2019-python-java-github-most-popular-language/)

![top](top-programming-languages-2024.png)

### Why did we choose Python for our Labautomation and why should you

Python is a free open source programming language with a simple syntax. Some advantages offered by Python are:

* Easy to learn (many educational resources)
* Runs on many platforms (from PC to raspberry pi to BBC microbit)
* Excellent numerical and scientific tools
* General purpose programming language (use it for simulations, lab-automation, scripts, GUIs...)
* Easy to read (difficult to write hard to understand code)
* It's fun!

For lab-automation it is particularly powerful that Python supports multiple programming styles including object-oriented and functional programming and provides an easy interface with C-code.

+++

## Further reading

* [Python Documentation](https://docs.python.org/3/)
* [The Python Tutorial](https://docs.python.org/3/tutorial/)
* [The Python Library Reference](https://docs.python.org/3/library/index.html)
* [Numpy Quickstart](https://docs.scipy.org/doc/numpy-1.15.0/user/quickstart.html)
* [Numpy for Matlab users](https://docs.scipy.org/doc/numpy-1.15.0/user/numpy-for-matlab-users.html)
* [Anaconda Documentation](https://docs.anaconda.com/)

+++

## How to install Python (Everyone starts from here)

One of the confusing Python aspects for beginner is how to get everything you need installed. 

Python is separated into the core language and *modules* which can be regarded as the equivalent to libraries in C.
The default Python language install contains a large standard library with modules covering everything from regular expressions to logging and webparsing. 
**However the important modules for numerical/scientific work are not part of the standard library**

While it is possible to install all required modules manually it is usually desired to instead install a Python distribution, which has all required packages already installed and allows adding packages in a consistent and compatible way.

There are several excellent python distributions available, in this shortcourse we will use [Miniforge](https://github.com/conda-forge/miniforge), as it is freely available without any restriction. If commercial support is desired a common popular distribution available is [Anaconda](https:///www.anaconda.com)




**We highly recommend using a python distribution like Minforge or Anaconda, particularly for Windows or MAC OS user**, whereas Linux users are also well served by the python packages offered by the Linux system package-manager.


+++

### Installation

Installing Miniforge is straight forward
* Download the Miniforge3 installer for the appropriate operating system from [here](https://github.com/conda-forge/miniforge#download)
* Follow the installation instructions on the web page
    - For Windows: Double click on the downloaded file and accept all defaults settings in the installer
    - For Linux: Run ```bash Miniforge3-Linux-x86_64.sh``` followed by ```~/miniforge3/condabin/conda init```
* Add additional packages using the ```conda``` command as described in section 3
* Additional instructions for more advanced installations can be found in [Miniforge install](https://github.com/robotology/robotology-superbuild/blob/master/doc/install-miniforge.md)
  
Installation of the following packages are required for the shortcourse:
- numpy
- scipy
- matplotlib
- spyder
- jupyterlab
- pyserial
- pyvisa

This can be achieved with the following command: ```conda install numpy scipy matplotlib spyder jupyterlab```

### [Alternative python environment for this class: google colab](https://colab.research.google.com/notebooks/welcome.ipynb)

+++

## Programming Python

Writing python programs can be done in two different ways
* interactively
* non-interactively

What to use for programming depends on the use case. For interactive programming you can directly start the python interpreter and begin programming. However, the default interpreter is relatively bare, the two recommended ways for interactive use, which are both installed with Anaconda, are:
* The IPython console (a much more feature-full interpreter)
* Jupyter notebooks (an interactive environment in the browser, what we are using in this course)

If you are writing modules to be included in other programs or scripts to be run non-interactively, you should use an editor. Many editors come with excellent python support, and to cover them all would be beyond this course.

### Interactive Development Environments (IDEs)

There are several excellent IDEs for Python. Three that we can recommend are:

* Spyder (free, open-source, included in Anaconda)
* [Pycharm](https://www.jetbrains.com/pycharm/) (Open source community edition and for cost professional edition, free licenses for academic use available)
* Jupyterlab (next generation jupyternotebooks)
* [Visual Studio Code](https://code.visualstudio.com/)
* Pyzo an IDE similar to Spyder that needs very little resources, which can be advantageous to run on small computer like the raspberrypis.

+++

## A first Python program

So lets start with the obligatory hello world program

```{code-cell} ipython3
print("Hello World!") ### try keyboard short-cut ('shift-enter')
```

## Python Control flow

One of the most controversial features of Python is that it uses white-space as part of its delimiting code blocks. Otherwise, the constructs are very similar to the ones encountered in C or other programming languages. 

+++

### while statement

```{code-cell} ipython3
i=0
while i < 10:
    print(i) ### use indention for code blocks
    i = i+1
```

### if then else

```{code-cell} ipython3
x = 1
if x > 0:
    print("greater")
else:
    print("lesser")   
```

```{code-cell} ipython3
x = 3
if x < 0:
    print("lesser")
elif x > 0 and x < 4:
    print("middle")
else:
    print("high")
```

### for loops 

```{code-cell} ipython3
for i in [1,2,3]:
    print(i)
```

When iterating over a range of numbers it's typically easiest to use the inbuild `range()` function

```{code-cell} ipython3
for i in range(3):
    print(i)
```

### The continue, break and else statements on a for loop

Three statements that can be associated with a loop are `continue`, `break` and (somewhat unintuitive) `else`

* The `break` statement breaks out of (exits) a running loop before the end condition is met

```{code-cell} ipython3
for i in range(10):
    if i > 4:
        break
    print(i)
print("i when breaking = {}".format(i))
```

* The `continue` statement continues with the next iteration of the loop, without executing the rest of the loop

```{code-cell} ipython3
for i in range(10):
    if i < 4:
        continue
    print(i)
```

* The `else` statement for `for` loops is relatively unknown. It executes when the loop has exited (ended) without encountering a break statement. This can be very useful for searches, see e.g. the following example from the official documentation

```{code-cell} ipython3
for n in range(2, 10):
    for x in range(2, n):
        if n % x == 0:
            print( n, 'equals', x, '*', n/x)
            break
    else:
        # loop fell through without finding a factor
        print(n, 'is a prime number')
```

## Python functions

Python functions are defined using the `def` statement. Everything to be executed in the function is indented

```{code-cell} ipython3
def fct(x):
    print(x)
```

```{code-cell} ipython3
fct(3)
```

To return values from a function we use the `return` statement, note it is possible to return more than one value

```{code-cell} ipython3
def fct(x):
    y = x+3
    return y, 3
```

```{code-cell} ipython3
fct(x)
```

Multiple function arguments are separated by commas. It is also possible to define arguments with default values (keyword arguments). All non-default arguments must come before default ones. 

```{code-cell} ipython3
def fct2(x, y, z=3):
    return x*y + z
```

```{code-cell} ipython3
print("1*2+10=",fct2(1,2))
print("1*2+3=",fct2(1,2,3))
print("1*2+5=",fct2(1,2, z=5))
```

## Python classes

While Python is a multi-paradigm programming language, object-oriented programming (OOP) always had a strong influence on its design. 

OOP is one of the most popular approaches for solving problems. In a nutshell it is about structuring your code into objects as the key component, where an object is defined by two characteristics:
* attributes
* behavior 

For example a rectangle is defined through the length of its two sides

Classes are central to OOP as they provide the blueprint for objects. In Python a Class is created with the `class` statement.

```{code-cell} ipython3
class Rectangle: # by convention classes are typically spelled in Camelcase
    x = 1.
    y = 2.
```

```{code-cell} ipython3
p = Rectangle()
print(type(p))
```

Class attributes are accessed through by using a '.' after the instance.

```{code-cell} ipython3
print(p.x)
p.y = 1.
print(p.y)
```

We can also associate behavior with objects by defining functions on the object, so-called *methods*

```{code-cell} ipython3
class Rectangle:
    x = 1.
    y = 2.
    def area(self): # note that self (a reference to the object itself) is always the first argument 
        return self.x*self.y
r = Rectangle()
r.area()
```

## An overview of Python data types

Let us have a brief overview (but incomplete) overview of some of the python types you will most commonly encounter. 

+++

## Strings

Because Python is a dynamic language, you do not have to declare variables, and they can also change types.

Strings are how text is represented in Python. You indicate a string through single (') or double (") quotes. 

*Note*: that since Python 3 strings are actually unicode entities, they therefore a somewhat more abstract concept and are not what is e.g. stored directly on disk. If you want to use actual byte representations you should use the *byte* type. In most cases one encounters in instrument automation, this does not matter, however.


+++

Let's create a string

```{code-cell} ipython3
a = "Hello World"
```

```{code-cell} ipython3
print(a)
```

One can do easy operations on strings such as splitting

```{code-cell} ipython3
greet, name = a.split(" ")
```

and concatenating

```{code-cell} ipython3
new_hello = greet + " Nick"
print(new_hello)
```

## Basic numeric types

Similar to most languages Python supports the typical numeric data types:
* integers
* floats
* complex numbers

Unlike C there is however no difference between e.g. `float` and `double` representations (not true for numpy). Also, there is a `long` integer type, which has unlimited precision. And a `bool` type which is essentially a subset of the integer type.

```{code-cell} ipython3
x = 2
y = 2.
z = 2. + 2.*1j
```

```{code-cell} ipython3
print(type(x))
print(type(y))
print(type(z))
```

Python (similar to Matlab) supports mixed type arithmetic. Thus, for in operation between two numerical types, the 'narrower' type will be converted to a higher type.

```{code-cell} ipython3
xx = x + y
print(xx)
print(type(xx))
```

```{code-cell} ipython3
yy = z + y
print(yy)
print(type(yy))
```

All typical operators are supported

```{code-cell} ipython3
x - y # subtraction
```

```{code-cell} ipython3
x + y # addition
```

```{code-cell} ipython3
x * y # multiplication
```

```{code-cell} ipython3
x / y # division
```

```{code-cell} ipython3
4.5 % y # remainder
```

```{code-cell} ipython3
4.5 // y # floor division
```

```{code-cell} ipython3
x ** 2 # power
```

**Caution**: The meaning of the `/` operator changed for integers between Python 2 and 3. `2/3` was an integer division. Now it generates a float.

```{code-cell} ipython3
2/3
```

You need to explicitly ask for integer division `//`.

```{code-cell} ipython3
2//3
```

## Lists and Dictionaries

Two other convenient types are Lists and Dictionaries. 

+++

### Lists

A list is essentially an ordered container of types and objects indicated by `[`.

```{code-cell} ipython3
x = [1, 2, 3, 4]
print(x)
print(x[0])
print(x[3])
```

*Note* that Python indices start at 0!

+++

Importantly the different elements do not have to be the same type

```{code-cell} ipython3
x2 = [1, 2. , 2+2j, [2, 'a', 'b']]
```

```{code-cell} ipython3
print(x2)
print(x2[2])
print(x2[3])
```

### Tuples

Tuples are a similar structure to lists, providing an ordered container.

+++

### Dictionaries

A dictionary is a mapping type (similar to a hash) and are cast with the `{`

```{code-cell} ipython3
c = {'a': 1, "b": 2., 4: 5}
```

```{code-cell} ipython3
print(c)
print(type(c))
```

```{code-cell} ipython3
print(c['a'])
```

```{code-cell} ipython3
print(c[4])
```

```{code-cell} ipython3
print(c['b'])
```

*Note*: Dictionaries are unordered. 

+++

## Numpy arrays

While initially tempting using lists to present arrays is generally not a good idea.

```{code-cell} ipython3
x = [1.]*100
y = [2.]*100
print(x)
print(y)
```

```{code-cell} ipython3
z=x*y
```

```{code-cell} ipython3
z=[]
for i in range(len(x)):
    z.append(x[i]*y[i])
print(z)
```

```{code-cell} ipython3
def listmult(x,y):
    z = []
    for i in range(len(x)):
        z.append(x[i]*y[i])
    return z
```

```{code-cell} ipython3
%timeit listmult(x,y)
```

This is very slow. To overcome the lack of an array type the *Numpy* module was born.

## Using modules

+++

Modules are similar to libraries in C. They can contain new classes, types and functions and in general are simply a Python file (or a set of files). To use a module we have to import it

```{code-cell} ipython3
import numpy
```

*Note* unlike e.g. Matlab imported modules live in their own namespace. Thus, to access a function from `numpy` I have to explicitly specify it

```{code-cell} ipython3
numpy.sin(numpy.pi/2)
```

There are several convenient way around naming imports and selective imports

```{code-cell} ipython3
import numpy as np # imports numpy and renames it to 'np', this is the suggested way to import numpy
np.sin(np.pi/2)
```

```{code-cell} ipython3
from numpy import sin
sin(np.pi/2)
```

```{code-cell} ipython3
from numpy import sin as nsin
nsin(np.pi/2)
# from numpy import * # is also possible but generally frowned upon because you could overwrite functions in your namespace
```

## Exercises (10 mins)

* Create a dictionary
* Iterate over the keys and values and print them
* Create a list
* Add an item to the list
* Remove an item from the list
* Combine the strings "hello" and "world"
* Try what happens when you modify a tuple

```{code-cell} ipython3

```
