---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.6
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# A  Demo on how to interface with an instrument
![demo](./Keysight_OFC_demo1.png)
+++

## First step

* create a resource manager object
* create an instance for Keysight Instruments

```{code-cell} ipython3
import time
import numpy as np
import matplotlib.pyplot as plt
import visa
rm = visa.ResourceManager()
```

## Second Step:
* Understand write and read functions with pyvisa
* Understand query function with pyvisa
* Understand SCPI command
* tips for quickly check SCPI command (use EXFO manual as an example)

![scpicommands](SCPI_Command_Reference.PNG)
### Connect to Laser Source

```{code-cell} ipython3
## Open Connection
tls = rm.open_resource('GPIB0::20::INSTR')
tls.timeout = 10000
## Reset Laser
tls.write('*RST')
complete = tls.query('*OPC?')
## Query ID
tls.query('*IDN?')
```

### Connect to Polarization Synthesizer

```{code-cell} ipython3
## Open Connection
polsyn = rm.open_resource('GPIB0::10::INSTR')
polsyn.timeout = 10000
## Reset Power Meter
polsyn.write('*RST')
complete = polsyn.query('*OPC?')
##Query ID
polsyn.query('*IDN?')
```

### Connect to Power Meter

```{code-cell} ipython3
## Open Connection
mppm = rm.open_resource('GPIB0::10::INSTR')
mppm.timeout = 10000
## Reset Power Meter
mppm.write('*RST')
complete = mppm.query('*OPC?')
##Query ID
mppm.query('*IDN?')
```

### Setup Laser Source

```{code-cell} ipython3
tls_wave = '1550e-9'
tls_power = '-10'
tls_mod = 1
tls_mod_freq = 100e-3
## Setup Wavelength
tls.write('SOUR0:WAVE ' + tls_wave)
## Setup Power
tls.write('SOUR0:POW:UNIT 0')
tls.write('SOUR0:POW '+ tls_power)
## Setup Modulation
## Turn On Output Power
tls.write('OUTP0:STAT 1')
```

### Setup Polarization Synthesizer - Scramble Polarization

```{code-cell} ipython3
polsyn.write('PCON:SCR:ENAB 1')
```

### Setup Power Meter

```{code-cell} ipython3
mppm_wave = '1550e-9'
## Setup Wavelength
mppm.write('SENS1:POW:WAV ' + mppm_wave)
```

### Query Single Channel Power - ASCII

```{code-cell} ipython3
mppm.query('READ1:POW?')
```

### Query All Channel Power - Binary Block

```{code-cell} ipython3
mppm.query_binary_values('READ:POW:ALL?','f',False)
```

### Setup Power Meter for Logging Operation

```{code-cell} ipython3
mppm_points = '1000'
mppm_avg = '1ms'

## Stop Previous Logging
mppm.write('SENs1:FUNC:STAT LOGG,STOP')
## Manual Range Mode
mppm.write('SENS1:POW:RANGE:AUTO 0')
mppm.write('SENS1:POW:GAIN:AUTO 0')
## Set Range
mppm.write('SENS1:POW:RANG 10DBM')
## CME - Continuous Measurement Trigger Mode
mppm.write('TRIG1:INPUT CME')
## Setup Logging Points and Averaging Time
mppm.write('SENS1:FUNC:PAR:LOGG %s,%s'%(mppm_points,mppm_avg))
## Change Units to Watt
mppm.write('SENS1:POW:UNIT 1')
```

### Start Logging, Trigger Measurement, Query for Logging Complete

```{code-cell} ipython3
## Start Logging
mppm.write('SENS1:FUNC:STAT LOGG,STAR')
## Trigger Logging
mppm.write('TRIG 1')
## Query Logging Complete
while True:
    progress = mppm.query('SENS1:FUNC:STATE?')
    print ('Chan1: ' + progress)
    time.sleep(1)
    if 'COMPLETE' in progress:
        break
```

### Query Logging Power Measurements - Binary Block

```{code-cell} ipython3
## Query Data
power = mppm.query_binary_values('SENS1:FUNC:RESULT?','f', False)
```

### Stop Logging

```{code-cell} ipython3
mppm.write('SENS1:FUNC:STAT LOGG,STOP')
```

### Disable Polarization Scrambling

```{code-cell} ipython3
polsyn.write('PCON:SCR:ENAB 0')
```

### Plot Logging Data

```{code-cell} ipython3
#Convert list to Array of Float to Plot Data
power_array = np.array(power)
#Plot Waveform
plt.plot(power_array)
plt.show()
```

### Close Instruments

```{code-cell} ipython3
mppm.close()
polsyn.close()
tls.close()
```
