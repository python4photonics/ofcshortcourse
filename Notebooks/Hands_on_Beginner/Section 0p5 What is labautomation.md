---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# A Lab automation Overview

Lab automation has become essential to perform complex laboratory experiments like for example high capacity fiber optics transmission experiments, where dozens of instruments have to be continuously adjusted during the measurements, which can take many hours to complete. 
Additionally, lab automation also helps freeing up operators from repetitive measurements, and can provide more consistent results by avoiding distractions that human operators are subject too. 
Finally, most modern measurement instruments provide ways to be controlled remotely, making lab automation even more compelling. 


## What instruments can we automate

Essentially every instrument that has some computer interface can be automated and even instruments that do not have a
computer interface can be automated if necessary, by e.g. using a stepper-motor that is controlled from a microcontroller
to turn a control button. Similarly, an instrument that can only be controlled via a proprietary graphical user interface, 
can if necessary be controlled by programmatically controlling mouse movements. However, in general it is much easier if
your instruments have a standard way to connect to a computer. We highly recommend that the ability of accessing the
instrument from a computer and an
[application programming interface (API)](https://en.wikipedia.org/wiki/API) that is well documented should be important
parameters for purchasing decisions.

There are many different interfaces and protocols that are used for connecting to instruments. The naming 
is often confusing as interfaces and protocols are sometimes used interchangeably and devices can be compatible with multiple systems.


### Serial

The RS-232 standard, which is commonly referred to as serial connection, is a standard that was originally introduced in the 1960s. It is still commonly encountered in 
laboratory instruments and being used for connecting to microcontrollers. Most modern PCs do not have an easily accessible RS-232 ports and instead connections are made
via USB-to-serial adapters. On the device side the connection is most commonly via the 9-pin DE-9 connector seen in {ref}`de9` 
```{figure} ./dsub.jpg
:name: de9
:width: 500px

DE-9 RS-232 connector (plug) source: [wikipedia](https://en.wikipedia.org/wiki/D-subminiature#DE-9)
```

The communication protocol can take different forms, some instruments take [SCPI](scpi) commands, while others
use home-grown protocols. With Python the [PySerial](https://pythonhosted.org/pyserial/) module gives programmatic access to the serial bus. [VISA](visa)-compliant 
devices can also be addressed via [PyVISA](https://pyvisa.readthedocs.io/en/latest/).

### GPIB (IEEE-488)

The GPIB (General Purpose Interface Bus) connector (see figure {ref}`gpibconn` is the most commonly encountered connector on old lab instruments. Originally developed in the 1960s it is still 
commonly found on many instruments such as oscilloscopes, optical spectrum analyzers or optical power meters. Technically, the hardware connector is defined via the IEEE-488.1 which also defines the basic protocol
parameters. The later IEEE-488.2 standard further introduced common commands and protocols (however it does not technically require GPIB hardware connectors). The {ref}`scpi` standard further extends IEEE-488 with 
instrument class specific control commands. The most common way to control GPIB instruments in Python is using [PyVISA](https://pyvisa.readthedocs.io/en/latest/), however you can also use [PyGPIB](https://pypi.org/project/pygpib/) 
to talk with GPIB devices. Note that your system will need to have the appropriate drivers installed, in particular USB-to-GPIB adapters often require specific drivers.
```{figure} ./gpibconn.jpg
:name: gpibconn
:width: 500px

GPIP connector source: [wikipedia]()
```

### USB

The USB test and measurement class (USBTMC) is a subclass of the USB interface for test and measurement instruments. It essentially specifies instruments that communicate using IEEE-488 type messages over USB. Many new test 
and measurement instruments such as oscilloscopes, spectrum analyzers etc. will communicate over USBTMC. Commands and syntax are typically of the same format as GPIB instruments and often use {ref}`scpi` type commands. In Python
access to USBTMC instruments is most easily done via [PyVISA](https://pyvisa.readthedocs.io/en/latest/) or alternatively using the [python-usbtmc](https://github.com/python-ivi/python-usbtmc) module. The advantage of the python-usbtmc
module over PyVISA is that it does no visa library is required.

(vxi)=
### Ethernet and VXI-11 and HiSLIP
Ethernet connections to test and measurement equipment most commonly uses the VXI-11 or HiSLIP protocols. They are specifications for communicating with instruments using the IEEE-488.2 command syntax over an ethernet/TCPIP connection. 
HiSLIP is the successor format to VXI-11 and primarily offers faster transfers via an asynchronous mode and shared and exclusive instrument locking. If available, HiSLIP is the preferable choice (see the [PyVISA chapter](<./Section 6 PyVISA for instrument control.md>))
for a more detailed discussion of how to select between the protocols). Typically, access to instruments would be via a VISA library and [PyVISA](https://pyvisa.readthedocs.io/en/latest/)
in Python. There also is a python implementation of the VXI-11 protocol available in the [python-vxi11](https://github.com/python-ivi/python-vxi11) module.

(scpi)=
### SCPI

The Standard Commands for Programmable Instruments (SCPI) standard is not a protocol or interface, but instead a standard for syntax and commands on top of the IEE-488 standard. It is commonly used by 
many test and measurement instruments from all the main manufacturers, independent of communication interface (USB, TCPIP, GPIB...). 

#### Command syntax

Commands are ASCII strings and are grouped in a tree structure, where different hierarchies are separated by a column. For example a measurement of the AC voltage would take the form of `MEASure:VOLTage:AC?` 
while a measurement of the DC current would be `MEASure:CURRent:DC?`. Where queries (asking for a value) are denoted by a question mark `?` and set commands contain the value to be set, without question mark. 
Commands are typically specified in upper-lower case format, however commands are case-insensitive and lower-case simply specifies that these letters can be omitted.
A detailed list of commands can be found in the instruments' remote control user manuals.  

(visa)=
### VISA

The Virtual Instrument Software Architecture (VISA) specification was defined in the mid 90s as an API for communication between test & measurement instruments and the computer. It is an industry standard implemented by several T&M companies including Keysight, Tektronix, National Instruments ...
VISA includes specifications for communicating with resources (mostly instruments) via I/O interfaces such as serial, GPIB, VXI, TCPIP (VXI-11), USBTMC and HiSLIP
The idea behind VISA is to achieve interface independence, i.e. it does not matter how the instrument is connected. Thus, it does not matter how an ASCII command string is send it is the same on each interface. 

In Python VISA is supported by the [PyVISA](https://pyvisa.readthedocs.io/en/latest/) module which provides a "pythonic" interface to VISA commands. Importantly PyVISA does not implement the VISA protocol itself, but provides a wrapper around a VISA library installed on the system, such as the NI-VISA libraries, the Tektronics VISA library, or the Keysights IO libraries. Additionally, there is also the  [PyVISA-py](https://github.com/pyvisa/pyvisa-py) lightweight pure python VISA implementation available, that is particularly useful for linux based computers or smaller computers like for example on a raspberry pi or similar.
PyVISA is discussed in more detail in the [Instrument control with PyVISA](<./Section 6 PyVISA for instrument control.md>) chapter.

### Others

Some instruments might use other interfaces and protocols than the ones mentioned above. It is particularly common that some instruments only provide access to their
instrument via a vendor supplied `dll` or library. Accessing these devices is covered in the advanced short-course.

## General Recommendation

The best way to connect to your instruments can often depend on a number of factors. Nevertheless, there are some general recommendations when selecting new instruments:

- Choose instruments with standard interfaces that are publicly documented
- Choose instruments based on publicly documented protocols 
- Avoid instruments that require custom vendor supplied libraries, as they are often tied to a particular operating system version that can quickly become obsolete
- Make sure that all functions needed for your experiments can be accessed by the supported remote control commands 

VISA libraries offer a simple way to connect via multiple different interfaces by using [PyVISA](https://pyvisa.readthedocs.io/en/latest/). 
If there is a VISA library available for your computer, and you use devices connected via different interfaces, it is often easiest to use PyVISA and VISA libraries. 
The operating system support in VISA libraries is however somewhat limited, especially if you are using Linux or OSX as operating system, where VISA libraries are often either unavailable or difficult to install. 
In that case [PyVISA-py](https://github.com/pyvisa/pyvisa-py), the pure python VISA implementation is preferred, and often
works quite well, but fails when no device drivers are available. This is the case, for example, for some of the USB-to-GPIB and GPIB-ethernet adapters (please check before acquiring new adapters). 
Additionally, if needed, instruments can be accessed without VISA by using protocol specific modules (pyserial, python-usbtmc). However, this will require
learning and using multiple protocol specific modules instead of just a single unified interface.

