---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"slideshow": {"slide_type": "slide"}}

# Introduction to the Labautomation Shortcourse

Welcome to the labautomation short course.

We are:
* Binbin Guan (Microsoft) [Binbin.Guan@microsoft.com](mailto:Binbin.Guan@microsoft.com)
* Jochen Schröder (Chalmers University of Technology) [jochen.schroeder@chalmers.se](mailto:jochen.schroeder@chalmers.se)
* Roland Ryf (Nokia Bell Labs) [roland.ryf@nokia-bell-labs.com](mailto:roland.ryf@nokia-bell-labs.com)

+++ {"slideshow": {"slide_type": "slide"}}

## Download Notes 

You can access the html version of these notes at [https://python4photonics.gitlab.io/ofcshortcourse/#](https://python4photonics.gitlab.io/ofcshortcourse/#)

Or follow this QR code:
![](../../_static/QR.png)

+++ {"slideshow": {"slide_type": "slide"}}

## Why this short-course?

* Over the last decade optical communication and photonics experiments have become increasingly involved. 
* It is common that state-of-the-art transmission experiments require measurements of more than hundred of channels at possibly ten or more different transmission distances. 
* This easily adds up to hundreds or thousands of individual measurement points. 
* __Nearly impossible__ to measure manually 

+++ {"slideshow": {"slide_type": "subslide"}}

Labautomation is now crucial to perform cutting edge experiments. 

* __But__ no real guidance.
    * no courses in engineering or science studies (maybe basic programming)
    * expensive courses for proprietary software 
    * everyone reinvents the wheel again 
    * we are research scientists *not* software developers, so often make rookie mistakes

+++ {"slideshow": {"slide_type": "subslide"}}

__This course__(SC469) aims to introduce and teach you about some of the best practices for lab automation that we discovered and learned about in our work as experimental 
photonics researchers and allow you to avoid some of the mistakes we made when starting off. 
* *Importantly* using free and open source tools (Python)

This is the introductory part of two courses on lab automation, If you want to learn more, we recommend SC487, which covers advanced topics for using python in photonic research and applications.

+++ {"slideshow": {"slide_type": "slide"}}

## History

* **2016** Nick Fontaine and myself (both big Python fans) started talking about ways to introduce people to open source tools for labautomation
    * brought Binbin on board
    * started the lab-automation hackathons  
* **2017** The first labautomation hackathon at OFC 2017
    * Further hackathons at ECOC,OFC, Photonic West and many others
* **2019** First OFC short course for hands-on lab automation with python
    * Over 70 attendees, very helpful and positive feedback
* **2020** OFC short course was cancelled due to Covid-19
* **2021** First virtual hands-on short course
* **2022** First in-person hands-on short course

Great to get people to talk and give some introductions of what is being done, but how can we help a student (or researcher) get started?

+++ {"slideshow": {"slide_type": "slide"}}

## Prior Knowledge

In this part of the course we do not assume any prior knowledge about Python or lab automation. However, some basic programming knowledge is assumed, so you should know what variables and functions are for example. 

+++ {"slideshow": {"slide_type": "slide"}}

## Learning Outcomes

While it is not possible to fully learn a new programming language, a new module ecosystem and related techniques on how to solve all your automation needs in a simple short course,  We aim to provide a global overview of the basic tools, ideas and patterns that we found useful in our work and that will give you a starting (or continuation) point to implement lab automation for your experimental needs. We present these ideas and patterns based on some example instruments (real and virtual) that are typically found in photonics laboratories. 

+++ {"slideshow": {"slide_type": "subslide"}}

At the end of this course we aim for you to have an understanding of:
* The basics of the Python programming language
* How to use the most basic scientific packages (numpy, scipy and matplotlib)
* Overview of some programming environments (IDEs)
* Using PyVISA to interface with VISA instruments (GPIB, serial, USB and Ethernet interfaces)
* How to plot your data 
* How to save data in Python

+++ {"slideshow": {"slide_type": "slide"}}

## Requirements

**Important:** Installation of the course requirement is covered in [installation](<./Section 3 Managing the anaconda distribution and module installation.md>), we highly recommend you to install the packages before the course.

### Python

This course is based on Python a free, open source programming language. 
The current version of Python is 3 (3.13), please do not use any of the older python 2 versions (@.X) as they are obsolete and no longer supported. 

While the two versions are generally very similar, they are not compatible, and some modifications are required to port python 2 programs to python 3.

**All course content is based on Python 3** 

Unless you absolutely require a module that is only available in Python 2 we highly recommend using Python 3 for all your projects, as Python 2 is no longer maintained.

Python 3 is available for all major operating systems, including Windows, OSX and Linux, and we assume that you have a Python 3 installed.

+++

### Jupyter

Jupyter (previously called ipython notebook) is an in-browser programming environments initially developed for Python, but now compatible with *R*, *Julia* and other programming languages. We use jupyter to generate the course content and recommend you to use it as your programming environment for this course.

### Numpy

Numpy is a Python module that implements an efficient array class for numerical computation. It is often used as building block for most scientific or engineering packages available in Python

+++

### Scipy

Scipy is a large package of additional scientific tools (such as scientific constants, and standard advanced scientific functions, linear algebra operations, signal processing functions and more). 

### PyVISA

The VISA standard and NI-VISA are the most common way to interface most lab-instruments. PyVISA was initially a python wrapper around the NI-VISA library, but has since gained an independent pure python implementation of the VISA standard. It provides a convenient interface to GPIB, serial port, Ethernet, and USB devices. 

+++ {"slideshow": {"slide_type": "subslide"}}

### Matplotlib

Matplotlib is arguably the best plotting module for producing publication ready graphics in Python.

### Bokeh

Bokeh is a plotting library for the web, and in contrast to matplotlib it is aimed at interactive graphics and graphical user interfaces (GUIs). Where matplotlib is 
often slow for interactive graphics, bokeh is typically much faster.

+++ {"slideshow": {"slide_type": "subslide"}}


### Cython

Cython is an enhanced Python dialect that can be directly compiled to C. It enables for a significant speed improvement to overcome bottlenecks in your Python code. We will also use it to interface to C-libraries.

### Ctypes

Ctypes is an alternative way to interface to native C-libraries from Python. It does not offer the same flexibility and performance as Cython, but can be easier to use, in particular very for simple interfacing tasks.

+++ {"slideshow": {"slide_type": "slide"}}

## Course Slides

All the course content is written in Jupyter Notebooks (we talk about this later). 

You can download them at: https://gitlab.com/python4photonics/ofcshortcourse|

You can also view a html version of the slides at https://python4photonics.gitlab.io/ofcshortcourse/index.html

+++ {"slideshow": {"slide_type": "slide"}}

## Schedule

1. [A Labautomation Overview](<./Section 0p5 What is labautomation.md>)
2. [Jupyter Notebooks](<./Section 1 Introducing Jupyter Notebooks.md>)
3. [Introduction to Python](<./Section 2 A basic introduction to Python.md>)
4. [Python Installation and Packages](<./Section 3 Managing the conda distribution and module installation.md>)
5. [A more detailed introduction to Numpy](<./Section 4 A more detailed Numpy Introduction.md>)
6. [How to plot using matplotlib and bokeh](<./Section 5 How to plot using matplotlib and bokeh.md>)
7. [PyVISA for instrument control](<./Section 6 PyVISA for instrument control.md>)
8. [Demo of interfacing with actual instruments](<./Section 7 Demo of interfacing with real instruments.md>)
9. [How to save and load data with Python](<./Section 8 How to save and load data with Python.md>)
10. [Final words](<./Section 9 Final Words.md>)

```{code-cell} ipython3

```
