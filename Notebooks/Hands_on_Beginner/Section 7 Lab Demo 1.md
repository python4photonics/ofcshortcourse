---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.6
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# OFC 2022 Demo 1 - Optical Power Measurement


## Create Resource Manager Object

```python
import time
import numpy as np
import matplotlib.pyplot as plt
import pyvisa as visa

rm = visa.ResourceManager()
```

##  Error Checking

```python
def err(inst):
    ## Query for Errors
    error_list = []
    i=0
    while True:
        inst.write(":SYSTem:ERRor?")
        error = inst.read()
        error_list.append(error)
        i = i + 1
        if 'No error' in error:
            break
    return error_list
```

## Define Instrument Slots

```python
tls_slot = 1
pm_slot = 2
```

## Connect to Laser Source and Power Meter

```python
## Open Connection
lms = rm.open_resource('GPIB4::22::INSTR')
lms.timeout = 10000
## Reset Laser
lms.write('*RST')
complete = lms.query('*OPC?')
## Query ID
id = lms.query('*IDN?')
print(id)
```

## Setup Laser Source

```python
tls_wave = '1550e-9'
tls_power = '10'

## Setup Wavelength
lms.write('SOUR{}:WAVE {}'.format(tls_slot,tls_wave))
## Setup Power
lms.write('SOUR{}:POW:UNIT 0'.format(tls_slot))
lms.write('SOUR{}:POW {}'.format(tls_slot,tls_power))
## Setup Modulation
## Turn On Output Power
lms.write('OUTP{}:STAT 1'.format(tls_slot))
err(lms)
```

## Setup Power Meter 

```python
pm_wave = '1550e-9'
## Setup Wavelength
lms.write('SENS{}:POW:WAV {}'.format(pm_slot,pm_wave))
err(lms)
```

## Query Single Channel Power - ASCII

```python
power = lms.query('READ{}:POW?'.format(pm_slot))
print('power: ' + power)
err(lms)
```

## Query All Channel Power - Binary Block

```python
lms.query_binary_values('READ:POW:ALL?','f',False)
print('power: ' + power)
err(lms)
```

## Close Instruments

```python
lms.close()
```

```python

```
