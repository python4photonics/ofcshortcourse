---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.6
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# OFC 2022 Demo 2 - Power Meter Logging 


## Create Resource Manager Object

```python

import time
import numpy as np
import matplotlib.pyplot as plt
import pyvisa as visa

rm = visa.ResourceManager()
```

## Error Checking

```python
def err(inst):
    ## Query for Errors
    error_list = []
    i=0
    while True:
        inst.write(":SYSTem:ERRor?")
        error = inst.read()
        error_list.append(error)
        i = i + 1
        if 'No error' in error:
            break
    return error_list
```

## Define Instrument Slots

```python
tls_slot = 1
pm_slot = 2
```

## Connect to Laser Source and Power Meter

```python
## Open Connection
lms = rm.open_resource('GPIB4::22::INSTR')
lms.timeout = 10000
## Reset Laser
lms.write('*RST')
complete = lms.query('*OPC?')
## Query ID
id = lms.query('*IDN?')
print(id)
```

## Setup Laser Source

```python
tls_wave = '1550e-9'
tls_power = '10'

## Setup Wavelength
lms.write('SOUR{}:WAVE {}'.format(tls_slot,tls_wave))
## Setup Power
lms.write('SOUR{}:POW:UNIT 0'.format(tls_slot))
lms.write('SOUR{}:POW {}'.format(tls_slot,tls_power))
## Setup Modulation
## Turn On Output Power
lms.write('OUTP{}:STAT 1'.format(tls_slot))
err(lms)
```

## Setup Power Meter 

```python
pm_wave = '1550e-9'
## Setup Wavelength
lms.write('SENS{}:POW:WAV {}'.format(pm_slot,pm_wave))
err(lms)
```

## Setup Power Meter for Logging Operation 

```python
pm_points = '1000'
pm_avg = '1ms'

## Stop Previous Logging
lms.write('SENs{}:FUNC:STAT LOGG,STOP'.format(pm_slot))
## Manual Range Mode
lms.write('SENS{}:POW:RANGE:AUTO 0'.format(pm_slot))
## Set Range
lms.write('SENS{}:POW:RANG 10DBM'.format(pm_slot))
## CME - Continuous Measurement Trigger Mode
lms.write('TRIG{}:INPUT CME'.format(pm_slot))
## Setup Logging Points and Averaging Time
lms.write('SENS{}:FUNC:PAR:LOGG {},{}'.format(pm_slot, pm_points, pm_avg))
## Change Units to Watt
lms.write('SENS{}:POW:UNIT 1'.format(pm_slot))
err(lms)
```

## Start Logging, Trigger Measurement, Query for Logging Complete

```python
## Start Logging
lms.write('SENS{}:FUNC:STAT LOGG,STAR'.format(pm_slot))
## Trigger Logging
lms.write('TRIG 1')
## Query Logging Complete
while True:
    progress = lms.query('SENS{}:FUNC:STATE?'.format(pm_slot))
    print ('Chan1: ' + progress)
    time.sleep(1)
    if 'COMPLETE' in progress:
        break
err(lms)
```

## Query Logging Power Measurements - Binary Block

```python
## Query Data
power = lms.query_binary_values('SENS{}:FUNC:RESULT?'.format(pm_slot),'f', False)
err(lms)
```

## Stop Logging 

```python
lms.write('SENS{}:FUNC:STAT LOGG,STOP'.format(pm_slot))
err(lms)
```

## Plot Logging Data

```python
power_array = np.array(power)
plt.clf()
plt.plot(10*np.log10(power_array/1e-3))
plt.show()
```

## Close Instruments

```python
lms.close()
```

```python

```
