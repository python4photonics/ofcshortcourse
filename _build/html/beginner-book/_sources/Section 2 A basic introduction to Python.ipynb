{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "a2c1e6a4",
   "metadata": {},
   "source": [
    "# A basic introduction to Python\n",
    "\n",
    "While it is not possible to give a full introduction to Python in the short time for this course, we aim here to give a brief introduction into some of the main features that are important for the rest of the course. If you have never used Python before, we recommend you to familiarize yourself with it before the course, by using one of the many excellent resources on the net, like the official [Python Tutorial](https://docs.python.org/3/tutorial/)\n",
    "\n",
    "## Learning Outcomes\n",
    "\n",
    "* A basic understanding of the Python programming language\n",
    "* Learn how to install Python using Miniforge\n",
    "* Learn about basic types and their usage\n",
    "* Learn how to write and use functions and classes\n",
    "* What are modules and how to use them\n",
    "* Why you should almost always use Numpy"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eec09bec",
   "metadata": {},
   "source": [
    "## Why Python\n",
    "\n",
    "### [Trend based on Github users](https://www.developer-tech.com/news/2019/nov/08/octoverse-2019-python-java-github-most-popular-language/)\n",
    "\n",
    "![top](top-programming-languages-2021.png)\n",
    "\n",
    "### Why did we choose Python for our Labautomation and why should you\n",
    "\n",
    "Python is a free open source programming language with a straight forward syntax. Some advantages offered by Python are:\n",
    "\n",
    "* Easy to learn (many educational resources)\n",
    "* Runs on many platforms (from PC to raspberry pi to BBC microbit)\n",
    "* Excellent numerical and scientific tools\n",
    "* General purpose programming language (use it for simulations, lab-automation, scripts, GUIs...)\n",
    "* Easy to read (difficult to write hard to understand code)\n",
    "* It's fun!\n",
    "\n",
    "For lab-automation it is particularly powerful that Python has a straight-forward approach to object-oriented and that it allows easy interfacing with C-code."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5e1c52f2",
   "metadata": {},
   "source": [
    "## Further reading\n",
    "\n",
    "* [Python Documentation](https://docs.python.org/3/)\n",
    "* [The Python Tutorial](https://docs.python.org/3/tutorial/)\n",
    "* [The Python Library Reference](https://docs.python.org/3/library/index.html)\n",
    "* [Numpy Quickstart](https://docs.scipy.org/doc/numpy-1.15.0/user/quickstart.html)\n",
    "* [Numpy for Matlab users](https://docs.scipy.org/doc/numpy-1.15.0/user/numpy-for-matlab-users.html)\n",
    "* [Anaconda Documentation](https://docs.anaconda.com/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "421fba40",
   "metadata": {},
   "source": [
    "## How to install Python (Everyone starts from here)\n",
    "\n",
    "One of the initially confusing aspects of Python is how to get everything you need installed. \n",
    "\n",
    "Python is separated into the core language and *modules* which can be regarded as the equivalent to libraries in C.\n",
    "The default Python language install contains a large standard library with modules covering everything from regular expressions to logging and webparsing. \n",
    "**However the important modules for numerical/scientific work are not part of the standard library**\n",
    "\n",
    "While it is possible to install all required modules manually it is usually desired to instead install a Python distribution, which has all languages already installed.\n",
    "\n",
    "There are several excellent python distributions available, in this shortcourse we will use [Miniforge](https://github.com/conda-forge/miniforge), as it is freely available without any restriction. If commercial support is desired a common popular distribution available is [Anaconda](https:///www.anaconda.com)\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "**We highly recommend using a python distribution like Minforge or Anaconda, particularly for Windows or MAC OS user**, whereas Linux users are often also well served by the python packages offered by the system package-manager."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2f3096f2",
   "metadata": {},
   "source": [
    "### Installation\n",
    "\n",
    "Installing Miniforge is straight forward\n",
    "* Download the Miniforge3 installer for the appropriate operating system from [here](https://github.com/conda-forge/miniforge#download)\n",
    "* Follow the installation instructions on the web page\n",
    "    - For Windows: Double click on the downloaded file and accept all defaults settings in the installer\n",
    "    - For Linux: Run ```bash Miniforge3-Linux-x86_64.sh``` followed by ```~/miniforge3/condabin/conda init```\n",
    "* Add additional packages using the ```conda``` command as described in section 3\n",
    "* Additional instructions for more advanced installations can be found in [Miniforge install](https://github.com/robotology/robotology-superbuild/blob/master/doc/install-miniforge.md)\n",
    "  \n",
    "Installation of the following packages are required for the shortcourse:\n",
    "- numpy\n",
    "- scipy\n",
    "- matplotlib\n",
    "- spyder\n",
    "- jupyterlab\n",
    "- pyserial\n",
    "- pyvisa\n",
    "\n",
    "This can be achieved with the following command: ```conda install numpy scipy matplotlib spyder jupyterlab```\n",
    "\n",
    "### [Alternative python environment for this class: google colab](https://colab.research.google.com/notebooks/welcome.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fecd8f49",
   "metadata": {},
   "source": [
    "## Programming Python\n",
    "\n",
    "Writing python programs can be done in two different ways\n",
    "* interactively\n",
    "* non-interactively\n",
    "\n",
    "What to use for programming depends on the user case. For interactive programming you can directly start the python interpreter and begin programming. However, the default interpreter is relatively bare, the two recommended ways for interactive use, which are both installed with Anaconda, are:\n",
    "* The IPython console (a much more feature-full interpreter)\n",
    "* Jupyter notebooks (an interactive environment in the browser, what we are using in this course)\n",
    "\n",
    "If you are writing modules to be included in other programs or scripts to be run non-interactively, you should use an editor. Many editors come with excellent python support, and to cover them all would be beyond this course.\n",
    "\n",
    "### Interactive Development Environments (IDEs)\n",
    "\n",
    "There are several excellent IDEs for Python. Three that we can recommend are:\n",
    "\n",
    "* Spyder (free, open-source, included in Anaconda)\n",
    "* [Pycharm](https://www.jetbrains.com/pycharm/) (Open source community edition and for cost professional edition, free licenses for academic use available)\n",
    "* Jupyterlab (next generation jupyternotebooks)\n",
    "* [Visual Studio Code](https://code.visualstudio.com/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6b88e46e",
   "metadata": {},
   "source": [
    "## A first Python program\n",
    "\n",
    "So lets start with the obligatory hello world program"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "38deeb4a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello World!\n"
     ]
    }
   ],
   "source": [
    "print(\"Hello World!\") ### try keyboard short-cut ('shift-enter')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "45086342",
   "metadata": {},
   "source": [
    "## Python Control flow\n",
    "\n",
    "One of the most controversial features of Python is that it uses white-space as part of its control flow. Otherwise, the constructs are very similar to ones encountered in C or other languages."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ad3a08ea",
   "metadata": {},
   "source": [
    "### while statement"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "ea41ef8b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0\n",
      "1\n",
      "2\n",
      "3\n",
      "4\n",
      "5\n",
      "6\n",
      "7\n",
      "8\n",
      "9\n"
     ]
    }
   ],
   "source": [
    "i=0\n",
    "while i < 10:\n",
    "    print(i) ### use indention for code blocks\n",
    "    i = i+1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2f43cd10",
   "metadata": {},
   "source": [
    "### if then else"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "cc76df78",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "greater\n"
     ]
    }
   ],
   "source": [
    "x = 1\n",
    "if x > 0:\n",
    "    print(\"greater\")\n",
    "else:\n",
    "    print(\"lesser\")   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "0be77110",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "middle\n"
     ]
    }
   ],
   "source": [
    "x = 3\n",
    "if x < 0:\n",
    "    print(\"lesser\")\n",
    "elif x > 0 and x < 4:\n",
    "    print(\"middle\")\n",
    "else:\n",
    "    print(\"high\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f1d83332",
   "metadata": {},
   "source": [
    "### for loops"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "aacd9193",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1\n",
      "2\n",
      "3\n"
     ]
    }
   ],
   "source": [
    "for i in [1,2,3]:\n",
    "    print(i)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e5fd8d0d",
   "metadata": {},
   "source": [
    "When iterating over a range of numbers it's typically easiest to use the inbuild `range()` function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "04a06555",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0\n",
      "1\n",
      "2\n"
     ]
    }
   ],
   "source": [
    "for i in range(3):\n",
    "    print(i)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e6f7351d",
   "metadata": {},
   "source": [
    "### The continue, break and else statements on a for loop\n",
    "\n",
    "Three statements that can be associated with a loop are `continue`, `break` and (somewhat unintuitive) `else`\n",
    "\n",
    "* The `break` statement breaks out of (exits) a running loop before the end condition is met"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "6e55f6aa",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0\n",
      "1\n",
      "2\n",
      "3\n",
      "4\n",
      "i when breaking = 5\n"
     ]
    }
   ],
   "source": [
    "for i in range(10):\n",
    "    if i > 4:\n",
    "        break\n",
    "    print(i)\n",
    "print(\"i when breaking = {}\".format(i))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "43d01e7b",
   "metadata": {},
   "source": [
    "* The `continue` statement continues with the next iteration of the loop, without executing the rest of the loop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "a041bb23",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "4\n",
      "5\n",
      "6\n",
      "7\n",
      "8\n",
      "9\n"
     ]
    }
   ],
   "source": [
    "for i in range(10):\n",
    "    if i < 4:\n",
    "        continue\n",
    "    print(i)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b73d6720",
   "metadata": {},
   "source": [
    "* The `else` statement for `for` loops is relatively unknown. It executes when the loop has exited (ended) without encountering a break statement. This can be very useful for searches, see e.g. the following example from the official documentation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "01a669c3",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2 is a prime number\n",
      "3 is a prime number\n",
      "4 equals 2 * 2.0\n",
      "5 is a prime number\n",
      "6 equals 2 * 3.0\n",
      "7 is a prime number\n",
      "8 equals 2 * 4.0\n",
      "9 equals 3 * 3.0\n"
     ]
    }
   ],
   "source": [
    "for n in range(2, 10):\n",
    "    for x in range(2, n):\n",
    "        if n % x == 0:\n",
    "            print( n, 'equals', x, '*', n/x)\n",
    "            break\n",
    "    else:\n",
    "        # loop fell through without finding a factor\n",
    "        print(n, 'is a prime number')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "47f465ef",
   "metadata": {},
   "source": [
    "## Python functions\n",
    "\n",
    "Python functions are defined using the `def` statement. Everything to be executed in the function is indented"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "406f13ae",
   "metadata": {},
   "outputs": [],
   "source": [
    "def fct(x):\n",
    "    print(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "f520711a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3\n"
     ]
    }
   ],
   "source": [
    "fct(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "06ee4c2e",
   "metadata": {},
   "source": [
    "To return values from a function we use the `return` statement, note it is possible to return more than one value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "ec6baa46",
   "metadata": {},
   "outputs": [],
   "source": [
    "def fct(x):\n",
    "    y = x+3\n",
    "    return y, 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "f63114f0",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(6, 3)"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "fct(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21793c40",
   "metadata": {},
   "source": [
    "Multiple function arguments are separated by commas. It is also possible to define arguments with default values (keyword arguments). All non-default arguments must come before default ones."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "a039377b",
   "metadata": {},
   "outputs": [],
   "source": [
    "def fct2(x, y, z=3):\n",
    "    return x*y + z"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "ecfdfb01",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1*2+10= 5\n",
      "1*2+3= 5\n",
      "1*2+5= 7\n"
     ]
    }
   ],
   "source": [
    "print(\"1*2+10=\",fct2(1,2))\n",
    "print(\"1*2+3=\",fct2(1,2,3))\n",
    "print(\"1*2+5=\",fct2(1,2, z=5))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6d43738e",
   "metadata": {},
   "source": [
    "## Python classes\n",
    "\n",
    "While Python is a mult-paradigm programming language, object-oriented programming (OOP) always had a strong influence on its design. \n",
    "\n",
    "OOP is one of the most popular approaches solving problems. In a nutshell it is about structuring your code into objects as the key component, where an object is defined by two characteristics:\n",
    "* attributes\n",
    "* behavior \n",
    "\n",
    "For example a rectangle is defined through the length of its two sides\n",
    "\n",
    "Classes are central to OOP as they provide the blueprint for objects. In Python a Class is created with the `class` statement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "9f756397",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Rectangle: # by convention classes are typically spelled in Camelcase\n",
    "    x = 1.\n",
    "    y = 2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "afb0f240",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class '__main__.Rectangle'>\n"
     ]
    }
   ],
   "source": [
    "p = Rectangle()\n",
    "print(type(p))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3f63de06",
   "metadata": {},
   "source": [
    "Class attributes are accessed through by using a '.' after the instance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "d9c3cf99",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1.0\n",
      "1.0\n"
     ]
    }
   ],
   "source": [
    "print(p.x)\n",
    "p.y = 1.\n",
    "print(p.y)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "814cbdb7",
   "metadata": {},
   "source": [
    "We can also associate behavior with objects by defining functions on the object, so-called *methods*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "c518c50b",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2.0"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "class Rectangle:\n",
    "    x = 1.\n",
    "    y = 2.\n",
    "    def area(self): # note that self (a reference to the object itself) is always the first argument \n",
    "        return self.x*self.y\n",
    "r = Rectangle()\n",
    "r.area()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "661e9eaa",
   "metadata": {},
   "source": [
    "## An overview of Python data types\n",
    "\n",
    "Let us have a brief overview (but incomplete) overview of some of the python types you will most commonly encounter."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0d055848",
   "metadata": {},
   "source": [
    "## Strings\n",
    "\n",
    "Because Python is a dynamic language, you do not have to declare variables, and they can also change types.\n",
    "\n",
    "Strings are how text is represented in Python. You indicate a string through single (') or double (\") quotes. \n",
    "\n",
    "*Note*: that since Python 3 strings are actually unicode entities, they therefore a somewhat more abstract concept and are not what is e.g. stored directly on disk. If you want to use actual byte representations you should use the *byte* type. In most cases one encounters in instrument automation, this does not matter, however."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3656cf1f",
   "metadata": {},
   "source": [
    "Let's create a string"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "791c3ee2",
   "metadata": {},
   "outputs": [],
   "source": [
    "a = \"Hello World\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "4a4b27fe",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello World\n"
     ]
    }
   ],
   "source": [
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7690d5c6",
   "metadata": {},
   "source": [
    "One can do easy operations on strings such as splitting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "c6a89e20",
   "metadata": {},
   "outputs": [],
   "source": [
    "greet, name = a.split(\" \")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4563b5c4",
   "metadata": {},
   "source": [
    "and concatenating"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "b3a53ebf",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello Nick\n"
     ]
    }
   ],
   "source": [
    "new_hello = greet + \" Nick\"\n",
    "print(new_hello)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81e528c8",
   "metadata": {},
   "source": [
    "## Basic numeric types\n",
    "\n",
    "Similar to most languages Python supports the typical numeric data types:\n",
    "* integers\n",
    "* floats\n",
    "* complex numbers\n",
    "\n",
    "Unlike C there is however no difference between e.g. `float` and `double` representations (not true for numpy). Also, there is a `long` integer type, which has unlimited precision. And a `bool` type which is essentially a subset of the integer type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "851fe589",
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 2\n",
    "y = 2.\n",
    "z = 2. + 2.*1j"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "e4ac00a5",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'int'>\n",
      "<class 'float'>\n",
      "<class 'complex'>\n"
     ]
    }
   ],
   "source": [
    "print(type(x))\n",
    "print(type(y))\n",
    "print(type(z))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6db73dd8",
   "metadata": {},
   "source": [
    "Python (similar to Matlab) supports mixed type arithmetic. Thus, for in operation between two numerical types, the 'narrower' type will be converted to a higher type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "id": "686ffdb3",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "4.0\n",
      "<class 'float'>\n"
     ]
    }
   ],
   "source": [
    "xx = x + y\n",
    "print(xx)\n",
    "print(type(xx))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "id": "ab1e6bed",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(4+2j)\n",
      "<class 'complex'>\n"
     ]
    }
   ],
   "source": [
    "yy = z + y\n",
    "print(yy)\n",
    "print(type(yy))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f009f41f",
   "metadata": {},
   "source": [
    "All typical operators are supported"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "id": "06c7cd33",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.0"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x - y # subtraction"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "id": "343a3baa",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4.0"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x + y # addition"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "id": "0e1f712a",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4.0"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x * y # multiplication"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "id": "16a5f7d7",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1.0"
      ]
     },
     "execution_count": 31,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x / y # division"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "id": "22e13815",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.5"
      ]
     },
     "execution_count": 32,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "4.5 % y # remainder"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "id": "563d315c",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2.0"
      ]
     },
     "execution_count": 33,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "4.5 // y # floor division"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "id": "4302b187",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4"
      ]
     },
     "execution_count": 34,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x ** 2 # power"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5a06de5e",
   "metadata": {},
   "source": [
    "**Caution**: The meaning of the `/` operator changed for integers between Python 2 and 3. `2/3` was an integer division. Now it generates a float."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "id": "3e43185a",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.6666666666666666"
      ]
     },
     "execution_count": 35,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "2/3"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cc68e53e",
   "metadata": {},
   "source": [
    "You need to explicitly ask for integer division `//`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "id": "b75207ca",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 36,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "2//3"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d1e13619",
   "metadata": {},
   "source": [
    "## Lists and Dictionaries\n",
    "\n",
    "Two other convenient types are Lists and Dictionaries."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "063b673d",
   "metadata": {},
   "source": [
    "### Lists\n",
    "\n",
    "A list is essentially an ordered container of types and objects indicated by `[`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "id": "26ee2e0f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 2, 3, 4]\n",
      "1\n",
      "4\n"
     ]
    }
   ],
   "source": [
    "x = [1, 2, 3, 4]\n",
    "print(x)\n",
    "print(x[0])\n",
    "print(x[3])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e972546d",
   "metadata": {},
   "source": [
    "*Note* that Python indices start at 0!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "07b4efbf",
   "metadata": {},
   "source": [
    "Importantly the different elements do not have to be the same type"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "id": "5a0385a7",
   "metadata": {},
   "outputs": [],
   "source": [
    "x2 = [1, 2. , 2+2j, [2, 'a', 'b']]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "id": "0f25fa0d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 2.0, (2+2j), [2, 'a', 'b']]\n",
      "(2+2j)\n",
      "[2, 'a', 'b']\n"
     ]
    }
   ],
   "source": [
    "print(x2)\n",
    "print(x2[2])\n",
    "print(x2[3])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "70165567",
   "metadata": {},
   "source": [
    "### Tuples\n",
    "\n",
    "Tuples are a similar structure to lists, providing an ordered container."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "92283b0c",
   "metadata": {},
   "source": [
    "### Dictionaries\n",
    "\n",
    "A dictionary is a mapping type (similar to a hash) and are cast with the `{`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "id": "d71f8846",
   "metadata": {},
   "outputs": [],
   "source": [
    "c = {'a': 1, \"b\": 2., 4: 5}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "id": "0cc35374",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'a': 1, 'b': 2.0, 4: 5}\n",
      "<class 'dict'>\n"
     ]
    }
   ],
   "source": [
    "print(c)\n",
    "print(type(c))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "id": "4531c326",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1\n"
     ]
    }
   ],
   "source": [
    "print(c['a'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 43,
   "id": "4f23cc24",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "5\n"
     ]
    }
   ],
   "source": [
    "print(c[4])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 44,
   "id": "bbb77f97",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2.0\n"
     ]
    }
   ],
   "source": [
    "print(c['b'])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1c3f71c8",
   "metadata": {},
   "source": [
    "*Note*: Dictionaries are unordered."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "878dfc8d",
   "metadata": {},
   "source": [
    "## Numpy arrays\n",
    "\n",
    "While initially tempting using lists to present arrays is generally not a good idea."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "id": "90d369ce",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]\n",
      "[2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0]\n"
     ]
    }
   ],
   "source": [
    "x = [1.]*100\n",
    "y = [2.]*100\n",
    "print(x)\n",
    "print(y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "id": "0f313362",
   "metadata": {},
   "outputs": [
    {
     "ename": "TypeError",
     "evalue": "can't multiply sequence by non-int of type 'list'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m                                 Traceback (most recent call last)",
      "Cell \u001b[0;32mIn[46], line 1\u001b[0m\n\u001b[0;32m----> 1\u001b[0m z\u001b[38;5;241m=\u001b[39m\u001b[43mx\u001b[49m\u001b[38;5;241;43m*\u001b[39;49m\u001b[43my\u001b[49m\n",
      "\u001b[0;31mTypeError\u001b[0m: can't multiply sequence by non-int of type 'list'"
     ]
    }
   ],
   "source": [
    "z=x*y"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2c0da243",
   "metadata": {},
   "outputs": [],
   "source": [
    "z=[]\n",
    "for i in range(len(x)):\n",
    "    z.append(x[i]*y[i])\n",
    "print(z)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7d1c6ffd",
   "metadata": {},
   "outputs": [],
   "source": [
    "def listmult(x,y):\n",
    "    z = []\n",
    "    for i in range(len(x)):\n",
    "        z.append(x[i]*y[i])\n",
    "    return z"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "451f343f",
   "metadata": {},
   "outputs": [],
   "source": [
    "%timeit listmult(x,y)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e75a7461",
   "metadata": {},
   "source": [
    "This is very slow. To overcome the lack of an array type the *Numpy* module was born.\n",
    "\n",
    "## Using modules"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11232e00",
   "metadata": {},
   "source": [
    "Modules are similar to libraries in C. They can contain new classes, types and functions and in general are simply a Python file (or a set of files). To use a module we have to import it"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "351d8562",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "801e854d",
   "metadata": {},
   "source": [
    "*Note* unlike e.g. Matlab imported modules live in their own namespace. Thus, to access a function from `numpy` I have to explicitly specify it"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24be65d4",
   "metadata": {},
   "outputs": [],
   "source": [
    "numpy.sin(numpy.pi/2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41e7d0e2",
   "metadata": {},
   "source": [
    "There are several convenient way around naming imports and selective imports"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7aa61316",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np # imports numpy and renames it to 'np', this is the defacto standard way to import numpy\n",
    "np.sin(np.pi/2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b2cf42c4",
   "metadata": {},
   "outputs": [],
   "source": [
    "from numpy import sin\n",
    "sin(np.pi/2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bdf62984",
   "metadata": {},
   "outputs": [],
   "source": [
    "from numpy import sin as nsin\n",
    "nsin(np.pi/2)\n",
    "# from numpy import * # is also possible but generally frowned upon because you could overwrite functions in your namespace"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e718aa49",
   "metadata": {},
   "source": [
    "## Exercises (10 mins)\n",
    "\n",
    "* Create a dictionary\n",
    "* Iterate over the keys and values and print them\n",
    "* Create a list\n",
    "* Add an item to the list\n",
    "* Remove an item from the list\n",
    "* Combine the strings \"hello\" and \"world\"\n",
    "* Try what happens when you modify a tuple"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3b6bfcb9",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,md:myst",
   "text_representation": {
    "extension": ".md",
    "format_name": "myst",
    "format_version": 0.13,
    "jupytext_version": "1.11.5"
   }
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.9"
  },
  "source_map": [
   13,
   28,
   49,
   60,
   80,
   105,
   128,
   134,
   136,
   142,
   146,
   151,
   155,
   163,
   171,
   175,
   178,
   182,
   185,
   193,
   199,
   203,
   208,
   212,
   221,
   227,
   232,
   234,
   238,
   244,
   246,
   250,
   255,
   259,
   273,
   279,
   282,
   286,
   290,
   294,
   302,
   308,
   319,
   323,
   327,
   329,
   333,
   335,
   339,
   342,
   353,
   359,
   363,
   367,
   373,
   377,
   381,
   385,
   389,
   393,
   397,
   401,
   405,
   407,
   411,
   413,
   417,
   419,
   425,
   431,
   436,
   440,
   444,
   448,
   452,
   458,
   464,
   468,
   473,
   477,
   481,
   483,
   487,
   493,
   500,
   504,
   511,
   519,
   521,
   527,
   531,
   533,
   537,
   539,
   543,
   548,
   553,
   557,
   569
  ]
 },
 "nbformat": 4,
 "nbformat_minor": 5
}