---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.6
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# OFC 2023 Demo 4 - Continuous Wavelength Sweep


## Create Resource Manager Object

```python

import time
import numpy as np
import matplotlib.pyplot as plt
import pyvisa as visa

rm = visa.ResourceManager()
```

## Error Checking

```python
def err(inst):
    ## Query for Errors
    error_list = []
    i=0
    while True:
        inst.write(":SYSTem:ERRor?")
        error = inst.read()
        error_list.append(error)
        i = i + 1
        if 'No error' in error:
            break
    return error_list
```

## Define Instrument Slots

```python
tls_slot = 0
pm_slot = 2
```

## Connect to Laser Source and Power Meter

```python
## Open Connection
lms = rm.open_resource('GPIB4::20::INSTR')
lms.timeout = 10000
## Reset Laser
lms.write('*RST')
complete = lms.query('*OPC?')
## Query ID
id = lms.query('*IDN?')
print(id)
```

## Setup Laser Source

```python
tls_wave = '1550e-9'
tls_power = '10'

## Setup Wavelength
lms.write('SOUR{}:WAVE {}'.format(tls_slot,tls_wave))
## Setup Power
lms.write('SOUR{}:POW:UNIT 0'.format(tls_slot))
lms.write('SOUR{}:POW {}'.format(tls_slot,tls_power))
## Setup Modulation
## Turn On Output Power
lms.write('OUTP{}:STAT 1'.format(tls_slot))
err(lms)
```

## Setup Power Meter 

```python
pm_wave = '1550e-9'
## Setup Wavelength
lms.write('SENS{}:POW:WAV {}'.format(pm_slot,pm_wave))
err(lms)
```

## Define Sweep Parameters

```python
start = 1545e-9 #meter
stop = 1565e-9  #meter
step = 1e-11 #meter
speed = 20e-9 #meter/sec
points = int((stop-start)/step)
print(points)
pm_range = 10
pm_avg = 0.0001
```

## Setup Continuous Sweep

```python

lms.write('TRIG:CONF LOOP')
lms.write('SOUR{}:POW:STAT 1'.format(tls_slot))
state = lms.query('SOUR{}:POW:STAT?'.format(tls_slot))
lms.write('TRIG{}:OUTP STF'.format(tls_slot))
lms.write('TRIG{}:INP SWS'.format(tls_slot))
lms.write('SOUR{}:WAV:SWE:STAR {}'.format(tls_slot,start))
err(lms)
lms.write('SOUR{}:WAV:SWE:STOP {}'.format(tls_slot,stop))
err(lms)
lms.write('SOUR{}:WAV:SWE:STEP {}'.format(tls_slot,step))
err(lms)
lms.write('SOUR{}:WAV:SWE:SPEED {}'.format(tls_slot,speed))
err(lms)
lms.write('SOUR{}:WAV:SWE:MODE CONT'.format(tls_slot))
err(lms)
lms.write('SOUR{}:WAV:SWE:LLOG 1'.format(tls_slot))
err(lms)
sweepcheck = lms.query('SOUR{}:WAV:SWE:CHEC?'.format(tls_slot))

if sweepcheck[0] == "0":
    pass
else:
    print(sweepcheck)
    
err(lms)
```

## TLS Power Check

```python
#tls.write("sour"+TLSslot+":wav:swe:pmax? "+start+"nm,"+stop+"nm")
max_power = lms.query('SOUR{}:WAV:SWE:PMAX? {}m,{}m'.format(tls_slot,start,stop)).strip()
lms.write('SOUR{}:POW {}dbm'.format(tls_slot,max_power))
err(lms)
```

## TLS PM Avg Time Check

```python

```

## Arm TLS Sweep

```python
lms.write('SOUR{}:WAV:SWE 1'.format(tls_slot))
err(lms)
```

## PM Logging Setup

```python
## Stop Previous Logging
lms.write('SENs{}:FUNC:STAT LOGG,STOP'.format(pm_slot))
## Manual Range Mode
lms.write('SENS{}:POW:RANGE:AUTO 0'.format(pm_slot))
## Set Range
lms.write('SENS{}:POW:RANG 10DBM'.format(pm_slot))
## SME - Single Measurement Trigger Mode
lms.write('TRIG{}:INPUT SME'.format(pm_slot))
## Setup Logging Points and Averaging Time
lms.write('SENS{}:FUNC:PAR:LOGG {},{}'.format(pm_slot, points, pm_avg))
log = lms.query('SENS{}:FUNC:PAR:LOGG?'.format(pm_slot))
print(log)
## Change Units to Watt
lms.write('SENS{}:POW:UNIT 1'.format(pm_slot))
## Start Logging
lms.write('SENS{}:FUNC:STAT LOGG,STAR'.format(pm_slot))
err(lms)
```

## Start Sweep and Query for Completion

```python
tlsFlag = 0
while tlsFlag == 0:
    lms.write('SOUR{}:WAV:SWE:FLAG?'.format(tls_slot))
    tlsFlag = int(lms.read())
    
# LambdaScan Program - Initiate sweep
lms.write('SOUR{}:WAV:SWE:SOFT'.format(tls_slot))

# LambdaScan Program - Check for PM logging complete
loggingStatus = "PROGRESS"
while loggingStatus.endswith("PROGRESS"):
    lms.write('SENS{}:FUNC:STAT?'.format(pm_slot))
    loggingStatus = lms.read().strip()
    time.sleep(0.5)
```

## Query Logging Wavelength & Power Measurements - Binary Block

```python
## Query Data
power = lms.query_binary_values('SENS{}:FUNC:RESULT?'.format(pm_slot),'f', False)
wavedata  = lms.query_binary_values('SOUR{}:READ:DATA? LLOG'.format(tls_slot),'d',False)
err(lms)
```

## Stop Logging 

```python
lms.write('SENS{}:FUNC:STAT LOGG,STOP'.format(pm_slot))
err(lms)
```

## Plot Logging Data

```python
power_array = np.array(power)
plt.clf()
plt.plot(10*np.log10(power_array/1e-3))
plt.show()
```

## Close Instruments

```python
lms.close()

```

```python

```
