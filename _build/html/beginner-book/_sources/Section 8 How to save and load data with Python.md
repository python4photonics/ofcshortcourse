---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Loading and Saving Data

Now that we talked a lot about how to control instruments using Python, let us talk about how to get data in and out of your programs.

## Learning Outcomes

Learn how to:

* load and save text files into your programs
* load and save numpy files
* serializing your objects
* interact with matlab (load and save matlab files)
* more advanced text file parsing
* how to deal with binary data
* advanced file-saving with tables

## Modules we will use

* numpy
* scipy.io
* pickle
* struct
* tables

+++

## Text files in Python

Let us first review how to read and write text files in "plain" Python (although this is often not what you want)

+++

Python has in-build support for file handling, in particular for dealing with text files. To open a file use the `open` command.

```{code-cell} ipython3
# lets start by reading from a previously generated text file
fp = open("testreadfile.txt", "r") # this open command opens a file in "read text mode" (the text is implicit) and returns a stream
```

```{code-cell} ipython3
# it is worthwhile to have a look at the open documentation in particular look at the different modes
help(open)
```

So what do we do with the `stream objects`?

```{code-cell} ipython3
help(fp)
```

```{code-cell} ipython3
# read a single line of the file
l1 = fp.readline()
print(l1)
```

```{code-cell} ipython3
# read another line
l2 = fp.readline()
print(l2)
```

```{code-cell} ipython3
# read a single character
c1 = fp.read(1)
print(c1)
```

```{code-cell} ipython3
# what happens if we read the line now?
l3 = fp.readline()
print(l3)
```

```{code-cell} ipython3
# let's put the line back together
l3 = c1+l3
print(l3)
```

```{code-cell} ipython3
# read to the end
s2 = fp.read()
print(s2)
```

```{code-cell} ipython3
# lets move back to the beginning
fp.seek(0)
```

```{code-cell} ipython3
# read text as lines
fp.readlines()
```

```{code-cell} ipython3
# close the file
fp.close()
```

### Saving to Text file with Python

The operations for saving to a text are very similar to how to read from files

```{code-cell} ipython3
fp = open("testwritefile.txt", "w")
fp.write("Test text")
fp.write("more text") # this will be concatenated just after the other text
fp.write("start a new line after this\n") # to start a new line put a end of line character
fp.writelines(["line1", "line2", "line3"]) # careful this name is confusing! it does not add newline characters!
fp.close() # IMPORTANT the data does get buffered (see the documentation) so will only fully be written when closing the object
```

## Files in Numpy

While the plain Python methods are very powerful, they can be also very cumbersome to use. In particular saving numerical data to text files

### Advantages
* can be opened pretty much everywhere

### Disadvantages
* file size is large (compared to binary data)
    * slow saving
    * slow loading
* formatting when writing
    * can lead to precision loss
    * slower
* cumbersome to read and write files 
    * need to convert to numerical values
    * delimiters 
    * comments

+++

### Saving numpy arrays

Numpy offers some basic file-handling capabilities for saving and writing numpy arrays.

```{code-cell} ipython3
import numpy as np

# generate a random array to save
a = np.random.randn(10**5)
```

```{code-cell} ipython3
# save the single array
np.save("testarray", a) # note that numpy automatically adds the "npy" extension
```

```{code-cell} ipython3
# loading works the same way
a2 = np.load("testarray.npy") #for loading we need to add the extension
np.all(a2==a) # note that comparing 2 floating point arrays with equality is not guaranteed to return True
```

```{code-cell} ipython3
# saving also preserves the shape
np.save("testarray2", a.reshape(10,10**4))
```

```{code-cell} ipython3
a3 = np.load("testarray2.npy")
a3.shape == (10, 10**4) # for integers we can use it
```

Saving more than a single file is slightly more involved. There is no equivalent to matlab `save`.

```{code-cell} ipython3
np.savez("twoarrays", array1=a, array2=a3) # if you want compression use np.savez_compressed
```

```{code-cell} ipython3
d = np.load("twoarrays.npz") # note the file extension. Essentially NPZ files are multiple npy files zipped together (uncompressed)
d.files
```

```{code-cell} ipython3
a_new = d['array1']
a3_new = d['array2']
print(np.allclose(a_new,a)) # np.allclose is better than comparing for equality
print(np.all(a3_new.shape==a3.shape))
```

### When to use?

* needs only Python (numpy) interoperability
* written data fits in to memory
    * do not need to extend
* need to save numpy arrays

+++

## Numpy and text files

Because it is so common to deal with data in row/column text files (or csv files), numpy has some powerful functions for loading and saving data in these formats. The two functions are `np.loadtxt` and `np.savetxt`

```{code-cell} ipython3
x = np.arange(10).reshape(5, 2) #  5 rows 2 columns
np.savetxt("columndata.txt", x) 
```

```{code-cell} ipython3
x2 = np.loadtxt("columndata.txt")
np.allclose(x2,x) 
```

The great thing about this is that it has a lot of parsing built in. Here's an old trace saved from an OSA.

```{code-cell} ipython3
fp = open("osadata.txt")
content = fp.read(370)
fp.close()
print(content)
```

```{code-cell} ipython3
%pylab inline
# reading with numpy loadtxt
wl, s1, s2 = np.loadtxt("osadata.txt", skiprows=24, delimiter=",", unpack=True) #unpack returns the different columns as separate arrays
plt.semilogy(wl, s1)
```

Much more powerful conversions are possible please read the [documentation](https://docs.scipy.org/doc/numpy/reference/generated/numpy.loadtxt.html)

+++

### When to use loadtxt/savetxt

`np.loadtxt`:
* everytime you need to load numerical data in text format
    * always try loadtxt first (much easier than manual parsing)

`np.savetxt`:
* avoid with large amounts of data
* use when you want the best interoperability (e.g. load into excel, but why would you after learning python 😀)

+++

## Interacting with Matlab

A lot of people are moving to Python from Matlab or are working with colleagues who use Matlab. Unfortunately Numpy does not allow reading from and writing to matlab files. **Fortunately** `scipy` does.

```{code-cell} ipython3
from scipy.io import loadmat, savemat

mfile = loadmat("chicom.mat")
print(mfile)
```

```{code-cell} ipython3
f = mfile["fTHz"].flatten() # matlab saves as 2-d files even if only 1D
chi = mfile["chi1111"].flatten()
fg,ax = plt.subplots(1,2, figsize=(16, 6))
ax[0].plot(f, chi.real)
ax[1].plot(f, chi.imag)
```

## How to save Python objects

Sometimes we do not just want to save numerical data or arrays, but Python objects. When we later read the file we want to have those objects back. 

One could write a function to save the object by separating into several e.g. numpy arrays and save using `np.savez`. To load we would then write another function that would write another function to generate the object back from a `np.load`ed file. 
* This is failure-prone
* We need to remember the format we saved in
* Changes to the object need to be reflected in the save function
* How to load old files (after the object changed)

From wikipedia:
> serialization (or serialization) is the process of translating data structures or object state into a format that can be stored (for example, in a file or memory buffer) or transmitted (for example, across a network connection link) and reconstructed later (possibly in a different computer environment)

Python provides a powerful serialization package called `pickle` which enables this.

```{code-cell} ipython3
# crate some class
class Rect(object):
    def __init__(self, x,y):
        self.x = x
        self.y = y
    def cal_area(self):
        return self.x*self.y
    
x = Rect(5, 7.)
```

```{code-cell} ipython3
import pickle
fp = open("testpickle.pic", "wb") # note the 'b' which means we are saving in byte format
pickle.dump(x, fp)
fp.close()
```

```{code-cell} ipython3
fp = open("testpickle.pic", "rb")
xnew = pickle.load(fp)
fp.close()
```

```{code-cell} ipython3
print(xnew)
print(type(xnew))
print(xnew.x)
print(xnew.y)
print(xnew.cal_area())
```

### Important points when using pickle

* Not all objects are picklable (see the [documentation](https://docs.python.org/3/library/pickle.html#what-can-be-pickled-and-unpickled) for what is and isn't)
    * highly recursive structures can be difficult and/or slow
    * you can customize the "picklability" of your classes
* Not space efficient (compress)
* can be slow (use different protocols)

```{code-cell} ipython3

```
