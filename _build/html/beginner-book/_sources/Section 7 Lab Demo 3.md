---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.6
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# OFC 2022 Demo 3 - Step Wavelength Sweep


## Create Resource Manager Object

```python

import time
import numpy as np
import matplotlib.pyplot as plt
import pyvisa as visa

rm = visa.ResourceManager()
```

## Error Checking

```python
def err(inst):
    ## Query for Errors
    error_list = []
    i=0
    while True:
        inst.write(":SYSTem:ERRor?")
        error = inst.read()
        error_list.append(error)
        i = i + 1
        if 'No error' in error:
            break
    return error_list
```

## Define Instrument Slots

```python
tls_slot = 1
pm_slot = 2
```

## Connect to Laser Source and Power Meter

```python
## Open Connection
lms = rm.open_resource('GPIB4::22::INSTR')
lms.timeout = 10000
## Reset Laser
lms.write('*RST')
complete = lms.query('*OPC?')
## Query ID
id = lms.query('*IDN?')
print(id)
```

## Setup Laser Source

```python
tls_wave = '1550e-9'
tls_power = '10'

## Setup Wavelength
lms.write('SOUR{}:WAVE {}'.format(tls_slot,tls_wave))
## Setup Power
lms.write('SOUR{}:POW:UNIT 0'.format(tls_slot))
lms.write('SOUR{}:POW {}'.format(tls_slot,tls_power))
## Setup Modulation
## Turn On Output Power
lms.write('OUTP{}:STAT 1'.format(tls_slot))
err(lms)
```

## Setup Power Meter 

```python
pm_wave = '1550e-9'
## Setup Wavelength
lms.write('SENS{}:POW:WAV {}'.format(pm_slot,pm_wave))
err(lms)
```

## Define Step Sweep Parameters

```python
start = 1545e-9
stop = 1565e-9
step = 1e-9
points = int((stop-start)/step)
print(points)

```

## Perform Step Sweep

```python
power = []

for i in range(0,points):
    print('iteration: {}'.format(i))
    tls_wave = start + i*step
    lms.write('SOUR{}:WAVE {}'.format(tls_slot,tls_wave))
    complete = lms.query('*OPC?').strip()
    my_power = lms.query('READ{}:POW?'.format(pm_slot)).strip()
    power.append(float(my_power))

err(lms)
```

## Plot Logging Data

```python
power_array = np.array(power)
plt.clf()
plt.plot(10*np.log10(power_array/1e-3))
plt.show()
```

## Close Instruments

```python
lms.close()
```

```python

```
