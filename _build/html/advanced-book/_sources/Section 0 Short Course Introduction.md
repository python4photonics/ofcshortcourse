---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"slideshow": {"slide_type": "slide"}}

# Labautomation Short Course

Welcome to the labautomation short course (advanced).

We are:
* Nicolas Fontaine (Nokia Bell Labs) [nicolas.fontaine@nokia-bell-labs.com](mailto:nicolas.fontaine@nokia-bell-labs.com)
* Binbin Guan (Microsoft) [Binbin.Guan@microsoft.com](mailto:Binbin.Guan@microsoft.com)
* Jochen Schröder (Chalmers University of Technology) [jochen.schroeder@chalmers.se](mailto:jochen.schroeder@chalmers.se)

+++ {"slideshow": {"slide_type": "slide"}}

## Download Notes 

You can access the html version of these notes at [https://python4photonics.gitlab.io/ofcshortcourse/#](https://python4photonics.gitlab.io/ofcshortcourse/#)

Or follow this QR code:
![](../../_static/QR.png)


+++ {"slideshow": {"slide_type": "slide"}}

## Why this short-course?

* Over the last decade optical communication and photonics experiments have become increasingly involved. 
* It is now not uncommon that state-of-the-art transmission experiments require measurments of more than a hundred of channel at possibly ten or more different transmission distances. 
* This easily adds up to hundreds to thousands of individual measurement points. 
* __Nearly impossible__ to measure manually 

+++ {"slideshow": {"slide_type": "subslide"}}

Labautomation is now crucial to perform cutting edge experiments. 

* __But__ no real guidance.
    * no courses in engineering or science studies (maybe basic programming)
    * expensive courses for proprietary software 
    * everyone reinvents the wheel again 
    * we are research scientists *not* software developers, so often make rookie mistakes

+++ {"slideshow": {"slide_type": "subslide"}}

__This course__(SC487) covers advanced topics for using python in photonic research and applications. 
* *Importantly* using free and open source tools (Python)

+++ {"slideshow": {"slide_type": "slide"}}

## History

* **2016** Nick and myself (both big Python fans) started talking about ways to introduce people to open source tools for labautomation
    * brought Binbin on board
    * started the lab-automation hackathons  
* **2017** The first labautomation hackathon at OFC 2017
    * Further hackathons at ECOC,OFC, Photonic West and many others
* **2019** First OFC short course for hands-on lab automation with python
    * Over 70 attendees, very helpful and positive feedback
* **2020** OFC short course was cancelled due to Covid-19
* **2021** First virtual short course!

Great to get people to talk and give some introductions of what is being done, but how can we help a student (or researcher) get started?

+++ {"slideshow": {"slide_type": "slide"}}

## Prior Knowledge

This course assumes that you have prior knowledge of using Python or significant experience with lab automation in other languages (e.g. Matlab), or that you have taken the introductory course (SC469).

+++ {"slideshow": {"slide_type": "slide"}}

## Learning Outcomes

It is not possible to cover all the details, but we will pick the most useful tools and tips that we have experience with.
*However* instead we aim to provide you with some tools, ideas and patterns that we found usefull in our work and that will give you a starting (or continuation) point to implement lab automation for your experimental needs. We present these ideas and patterns based on some example instruments (real and virtual) that are typical for a photonics laboratory. 

+++ {"slideshow": {"slide_type": "subslide"}}

At the end of this course we aim for you to have an understanding of:
* How to write classes, decorators
* How to use ipywidgets for lab automation visualization
* How to write object oriented code for interfacing to lab instruments
* How to interface with c-code in Python
* How to speed up your code
* How to use git version control to sustainably manage your projects

+++ {"slideshow": {"slide_type": "slide"}}

## Requirements

**Important:** We assume you already have a Python environment to work with. We prefer to use Anaconda distribution for this course.

### Python

This course is based on programing in Python a free, open source programming language. 

Two versions of Python exists:
* Python 2 (legacy)
* Python 3 






+++

While they are generally very similar, unless care is taken code is not compatible across the two versions

**All course content is based on Python 3** 

Unless you absolutely require a module that is only available in Python 2 we highly recommend using Python 3 for all your projects, as Python 2 is unmaintened in 2020.

Python 3 is available for all major operating systems, including Windows, OSX and Linux and we assume that you have a Python 3 installed.

+++

### Jupyter

Jupyter (previously ipython notebook) is a in-browser programming enviroment initially developed for Python, but now compatible with *R*, *Julia* and others. We used jupyter to generate the course content and recommend to use it as your programming enviroment for this course.

### Numpy

Numpy is a Python module that implements an array class as well as efficient operations and functions for this class. It is absolutely central to almost any scientific or engineering package in Python

+++

### Scipy

Scipy is a large package of additional scientific tools (such as linear algebra operations, signal processing functions etc.). 

### PyVISA

The VISA standard and NI-VISA are probably still the most universal way of interfacing to most lab-instruments. PyVISA was initially a python wrapper around the NI-VISA library, but has since gained an independent implementation of VISA. It provides a convenient interface to GPIB, serial port, ethernet and USB devices. 

+++ {"slideshow": {"slide_type": "subslide"}}

### Matplotlib

Matplotlib is arguably the best plotting module for producing publication ready graphics in Python

### Bokeh

Bokeh is a plotting library for the browser, in contrast to matplotlib it is aimed more at interactive graphics and graphical user interfaces (GUIs). Where matplotlib is 
often slow for interactive graphics, bokeh is often much faster.

+++ {"slideshow": {"slide_type": "subslide"}}


### Cython

Cython is an enhanced Python dialect that can be directly compiled to C. It enables to significantly speed up performance bottlenecks in your Python code. We will use it to interface to C-libraries.

### Ctypes

Ctypes is another way to interface to native C-libraries from Python. It does not offer the same flexiblity and performance as Cython, but is arguably easier to use, in particular for simple interfaces.

+++ {"slideshow": {"slide_type": "slide"}}

## Course Slides

All the course content is written in Jupyter Notebooks (we talk about this later). 

You can download them at: https://gitlab.com/python4photonics/ofcshortcourse
 
You can also view a generated version of the material online at https://python4photonics.gitlab.io/ofcshortcourse/index.html

+++ {"slideshow": {"slide_type": "slide"}}

## Schedule

0. [A quick recap of PyVISA](<./Section 1 A quick recap on PyVISA.md>)
1. [Graphical User Interfaces](<./Section 2 Graphical User Interfaces.md>)
2. [Object-Oriented programming for instrument control](<./Section 3 Object-Oriented Programming for Instrument Control.md>)
3. [Python Decorators by example logging instrument calls](<./Section 4 Python Decorators by example logging instrument calls.md>)
4. [Interfacing with C code](<./Section 5 Interfacing with C-Code.md>)
5. [Pythran and cython to speed up python code](<./Section 6 Pythran and Cython to Speed up Python Code.md>)
6. [Using git for tracking your projects](<./Section 8 Using Git for version control.md>)
7. [Automated testing with pytest](<./Section 9 Automated Testing With Pytest.md>)
8. [A quick intro into GPU programming](<./Section 10 An Intro to GPU Programming.md>)
9. [SOP alignment demo (supplementary)](<./Section 3p5 Instruments Demo.md>)
10. [An IQ modulator demo (supplementary)](<./Section 7 An IQ Modulator Demo.md>)
11. Final Words

```{code-cell} ipython3

```
