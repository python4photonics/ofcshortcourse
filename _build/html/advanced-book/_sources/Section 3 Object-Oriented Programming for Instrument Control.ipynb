{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f45efa36",
   "metadata": {},
   "source": [
    "# Object-Oriented Programming for Instrument Control\n",
    "\n",
    "You can certainly use the general pyvisa API for communicating with your devices. However, you quickly end up in a situation where you have lots of different VISA calls and even if you put them in functions you end up with a lot of functions. This is not really sustainable if you want to reuse your instrument control. So let us look at how to make a reusable library for these instruments and how to use them together. \n",
    "\n",
    "For this section we will use object-oriented programming, which is ideal for writing controls for lab instruments."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4f985e44",
   "metadata": {},
   "source": [
    "## What is Object-Oriented Programming\n",
    "\n",
    "from wikipedia:\n",
    "> Object-oriented programming (OOP) is a programming paradigm based on the concept of \"objects\", which may contain data, in the form of fields, often known as attributes; and code, in the form of procedures, often known as methods. A feature of objects is that an object's procedures can access and often modify the data fields of the object with which they are associated (objects have a notion of \"this\" or \"self\"). In OOP, computer programs are designed by making them out of objects that interact with one another\n",
    "\n",
    "In other words OOP is about creating object with certain characteristics and methods that can be used to access and control the behaviour of these objects. This also explains why it lends itself to instrument-control, the instruments are actually \"real\" objects that we are controlling.\n",
    "\n",
    "Python is very well suited for OOP, because while it can be used for programming in many different ways, it was designed with OOP in mind and many concepts in Python are based on OOP."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0ccd8bd5",
   "metadata": {},
   "source": [
    "## Learning Outcomes\n",
    "\n",
    "* basic understanding of object-oriented programming\n",
    "    * basic inheritance\n",
    "* how to build lab instruments using object-oriented programming\n",
    "* how to put several instruments together \n",
    "\n",
    "\n",
    "### Things we don't cover\n",
    "\n",
    "Some of the following concepts make OOP very powerful. We will not cover them today, but we highly recommend you read up on them!\n",
    "\n",
    "* metaprogramming\n",
    "* duck-typing\n",
    "\n",
    "**Important** While we will be using jupyter notebooks for writing the object here, generally you do not want to have your instrument control libraries in a notebook. Notebooks are great for initial experimentation, analysis and debugging, but you can not easily include (or import) the code from your notebook into other programs. Instead of copy and pasting your instruments from notebook to notebook, put them into a separate Python file that you can then import like any other Python module."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dde672d9",
   "metadata": {},
   "source": [
    "## An Tektronix DPO scope object\n",
    "\n",
    "In this section we will write an object to represent a Tektronix DPO70000 series scope. \n",
    "\n",
    "Along the way we will explain some of the OOP concepts in connection to Python. Note that we will only write an interface to a selection of the scope commands (the API of the scope is extremely large).\n",
    "\n",
    "We start with a generic instrument object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "39125f7e",
   "metadata": {},
   "outputs": [],
   "source": [
    "class LabInstrument(object): # Note classes are by convention written in CamelCase\n",
    "    def __init__(self, visa_instrument):\n",
    "        \"\"\"\n",
    "        Initialise a Tektronix DPO scope\n",
    "        \n",
    "        Parameters\n",
    "        ----------\n",
    "        \n",
    "        visa_instrument : visa_object\n",
    "            the visa resource for the instrument\n",
    "        \"\"\"\n",
    "        self.instrument = visa_instrument"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2e27f9d1",
   "metadata": {},
   "source": [
    "So what is happing in the above?\n",
    "\n",
    "The `class` command creates an object class (the `(object)` indicates it is a subclass of the object class). This is basically the \"blueprint\" for a type of object. For example we could have the class of humans, which could possibly have subclasses women, men, children, adults, which all have different characteristics. It is possible that objects are members of several classes. \n",
    "\n",
    "The `__init__` is a method. Methods are functions that belong to an object. The first parameter to a method is (almost) always the `self` parameter, which is the object this function belongs to. The `__init__` method is one of several *special* methods, it gets called when we first initialise an object. \n",
    "\n",
    "Inside the `__init__` method we add three attributes to the object\n",
    "\n",
    "Let's create the object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "18f7cca9",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyvisa as visa\n",
    "import numpy as np\n",
    "rm = visa.ResourceManager()\n",
    "scope_inst = rm.open_resource(\"TCPIP0::192.168.1.195::INSTR\")\n",
    "dpo = LabInstrument(scope_inst)\n",
    "print(dpo.instrument.query(\"*IDN?\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "55e7aabd",
   "metadata": {},
   "source": [
    "So far the instrument does not do anything so let us add some functionality to the instrument"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ce994186",
   "metadata": {},
   "outputs": [],
   "source": [
    "class LabInstrument(object):\n",
    "    def __init__(self, visa_instrument):\n",
    "        \"\"\"\n",
    "        Initialise a Tektronix DPO scope\n",
    "        \n",
    "        Parameters\n",
    "        ----------\n",
    "        \n",
    "        visa_instrument : visa_object\n",
    "            the visa resource for the instrument\n",
    "        \"\"\"\n",
    "        self.instrument = visa_instrument\n",
    "    \n",
    "    def idn(self):\n",
    "        \"\"\"\n",
    "        Return instrument ID\n",
    "        \"\"\"\n",
    "        return self.instrument.query(\"*IDN?\")\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "47bb2ae2",
   "metadata": {},
   "outputs": [],
   "source": [
    "dpo = LabInstrument(scope_inst)\n",
    "print(dpo.idn())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e25dbe67",
   "metadata": {},
   "source": [
    "Here we see how to use the property decorator to convert a method into an attribute (in this case a read-only attribute)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f491d5f3",
   "metadata": {},
   "outputs": [],
   "source": [
    "class LabInstrument(object):\n",
    "    def __init__(self, visa_instrument):\n",
    "        \"\"\"\n",
    "        Initialise a Tektronix DPO scope\n",
    "        \n",
    "        Parameters\n",
    "        ----------\n",
    "        \n",
    "        visa_instrument : visa_object\n",
    "            the visa resource for the instrument\n",
    "        \"\"\"\n",
    "        self.instrument = visa_instrument\n",
    "    \n",
    "    @property #we talk about decorators later\n",
    "    def idn(self):\n",
    "        \"\"\"\n",
    "        Return instrument ID\n",
    "        \"\"\"\n",
    "        return self.instrument.query(\"*IDN?\")\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "70f7efd5",
   "metadata": {},
   "outputs": [],
   "source": [
    "dpo = LabInstrument(scope_inst)\n",
    "print(dpo.idn)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dafca9b4",
   "metadata": {},
   "source": [
    "We really don't want to talk to the instrument everytime we want to get the idea. Let's implement a poor man's cache."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c83f1d1d",
   "metadata": {},
   "outputs": [],
   "source": [
    "class LabInstrument(object):\n",
    "    def __init__(self, visa_instrument):\n",
    "        \"\"\"\n",
    "        Initialise a Tektronix DPO scope\n",
    "        \n",
    "        Parameters\n",
    "        ----------\n",
    "        \n",
    "        visa_instrument : visa_object\n",
    "            the visa resource for the instrument\n",
    "        \"\"\"\n",
    "        self.instrument = visa_instrument\n",
    "    \n",
    "    @property\n",
    "    def idn(self):\n",
    "        \"\"\"\n",
    "        Return instrument ID\n",
    "        \"\"\" \n",
    "        try: #poor man's cache\n",
    "            self._idn\n",
    "        except AttributeError:\n",
    "            self._idn = self.instrument.query(\"*IDN?\") # the leading _ indicates private attributes\n",
    "            return self._idn\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "228b01b9",
   "metadata": {},
   "outputs": [],
   "source": [
    "if len(rm.list_opened_resources()) == 0:\n",
    "    scope_inst = rm.open_resource(\"TCPIP0::192.168.1.195::INSTR\")\n",
    "dpo = LabInstrument(scope_inst)\n",
    "print(dpo.idn)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "79e238c5",
   "metadata": {},
   "source": [
    "## Inheritance and more sophisticated classes\n",
    "\n",
    "Let's say we want to build on this initial LabInstrument class to make some more sophisticated instruments, which however all have an idn property. We can use inheritance. \n",
    "\n",
    "We are now going to build the scope class"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "27a6ec3a",
   "metadata": {},
   "outputs": [],
   "source": [
    "class DPO70000SX(LabInstrument): #here we indicate that we inherit from the LabInstrument class\n",
    "    def __init__(self, visa_instrument):\n",
    "        \"\"\"\n",
    "        Initialise a Tektronix DPO scope\n",
    "        \n",
    "        Parameters\n",
    "        ----------\n",
    "        \n",
    "        visa_instrument : visa_object\n",
    "            the visa resource for the instrument\n",
    "        \"\"\"\n",
    "        super().__init__(visa_instrument) #super allows us to call the method of parent class\n",
    "    \n",
    "    @property # this property gives us the sampling rate of the scope\n",
    "    def fs(self):\n",
    "        return float(self.instrument.query(\"HOR:MODE:SAMPLERATE?\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "58e323f6",
   "metadata": {},
   "outputs": [],
   "source": [
    "if len(rm.list_opened_resources()) == 0:\n",
    "    scope_inst = rm.open_resource(\"TCPIP0::192.168.1.195::INSTR\")\n",
    "dpo = DPO70000SX(scope_inst)\n",
    "print(\"IDN = {}\".format(dpo.idn))\n",
    "print(\"Sampling rate = {:f} GHz\".format(dpo.fs/1e9))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ccde0ac",
   "metadata": {},
   "source": [
    "### Read/write properties \n",
    "\n",
    "Unlike the IDN we likely want to change the sampling rate. For this we can make use of the `property.setter` decorator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "99768eef",
   "metadata": {},
   "outputs": [],
   "source": [
    "class DPO70000SX(LabInstrument):\n",
    "    def __init__(self, visa_instrument):\n",
    "        \"\"\"\n",
    "        Initialise a Tektronix DPO scope\n",
    "        \n",
    "        Parameters\n",
    "        ----------\n",
    "        \n",
    "        visa_instrument : visa_object\n",
    "            the visa resource for the instrument\n",
    "        \"\"\"\n",
    "        self.instrument = visa_instrument\n",
    "       \n",
    "    @property\n",
    "    def fs(self):\n",
    "        return float(self.instrument.query(\"HOR:MODE:SAMPLERATE?\"))\n",
    "    \n",
    "    @fs.setter\n",
    "    def fs(self, fs):\n",
    "        self.instrument.write(\"HOR:MODE:SAMPLERATE {:e}\".format(fs))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "68ed3103",
   "metadata": {},
   "outputs": [],
   "source": [
    "if len(rm.list_opened_resources()) == 0:\n",
    "    scope_inst = rm.open_resource(\"TCPIP0::192.168.1.195::INSTR\")\n",
    "dpo = DPO70000SX(scope_inst)\n",
    "print(\"IDN = {}\".format(dpo.idn))\n",
    "print(\"Sampling rate = {:.1f} GHz\".format(dpo.fs/1e9))\n",
    "dpo.fs = dpo.fs/2\n",
    "print(\"Sampling rate = {:.1f} GHz\".format(dpo.fs/1e9))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "334078ac",
   "metadata": {},
   "source": [
    "Finally, the advantage of OOP is that you can easily hide more complex calculations inside the methods, but they remain associated with the particular instrument"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1d104346",
   "metadata": {},
   "outputs": [],
   "source": [
    "class DPO70000SX(LabInstrument):\n",
    "    def __init__(self, visa_instrument):\n",
    "        \"\"\"\n",
    "        Initialise a Tektronix DPO scope\n",
    "        \n",
    "        Parameters\n",
    "        ----------\n",
    "        \n",
    "        visa_instrument : visa_object\n",
    "            the visa resource for the instrument\n",
    "        \"\"\"\n",
    "        self.instrument = visa_instrument\n",
    "    \n",
    "    @property\n",
    "    def idn(self):\n",
    "        \"\"\"\n",
    "        Return instrument ID\n",
    "        \"\"\" \n",
    "        try: #poor man's cache\n",
    "            self._idn\n",
    "        except AttributeError:\n",
    "            self._idn = self.instrument.query(\"*IDN?\") # the leading _ indicates private attributes\n",
    "            return self._idn\n",
    "    \n",
    "    @property\n",
    "    def fs(self):\n",
    "        return float(self.instrument.query(\"HOR:MODE:SAMPLERATE?\"))\n",
    "    \n",
    "    @fs.setter\n",
    "    def fs(self, fs):\n",
    "        self.instrument.write(\"HOR:MODE:SAMPLERATE {:e}\".format(fs))\n",
    "                                            \n",
    "    def get_data(self, channel_no, plot=False):\n",
    "        self.instrument.write(\":data:source ch{:d}\".format(channel_no))\n",
    "        self.instrument.write(\":data:encdg fastest\")\n",
    "        self.instrument.write(\":data:width 2\")\n",
    "        self.instrument.write(\":data:start 1\")\n",
    "        self.instrument.write(\":data:stop 1e10\")\n",
    "\n",
    "        # Read preamble\n",
    "        pre = self.instrument.query(\":wfmoutpre?\").split(';')\n",
    "\n",
    "        acq_format = pre[7].strip().upper()\n",
    "        points = int(pre[6])\n",
    "        point_size = int(pre[0])\n",
    "        point_enc = pre[2].strip().upper()\n",
    "        point_fmt = pre[3].strip().upper()\n",
    "        byte_order = pre[4].strip().upper()\n",
    "        x_unit = pre[8][1:-1]\n",
    "        y_unit = pre[12][1:-1]\n",
    "        x_increment = float(pre[9])\n",
    "        x_origin = float(pre[10])\n",
    "        x_reference = int(float(pre[11]))\n",
    "        y_increment = float(pre[13])\n",
    "        y_reference = int(float(pre[14]))\n",
    "        y_origin = float(pre[15])\n",
    "        \n",
    "        raw = self.instrument.query_binary_values(\"CURVE?\", datatype=\"b\", container=np.array)\n",
    "        x = (np.arange(raw.size) - x_reference) * x_increment + x_origin\n",
    "        y = (raw - y_reference) * y_increment + y_origin\n",
    "        if plot:\n",
    "            import matplotlib.pylab as plt\n",
    "            fig = plt.figure()\n",
    "            plt.plot(x, y, lw=1)\n",
    "            plt.xlabel(\"time [{}]\".format(x_unit))\n",
    "            plt.ylabel(\"Amplitude [{}]\".format(y_unit))\n",
    "            plt.show()\n",
    "        return x,y"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f6a59256",
   "metadata": {},
   "outputs": [],
   "source": [
    "if len(rm.list_opened_resources()) == 0:\n",
    "    scope_inst = rm.open_resource(\"TCPIP0::192.168.1.195::INSTR\")\n",
    "dpo = DPO70000SX(scope_inst)\n",
    "print(\"IDN = {}\".format(dpo.idn))\n",
    "dpo.get_data(1, plot=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f698af34",
   "metadata": {},
   "outputs": [],
   "source": [
    "rm.list_opened_resources()[0].close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9fd03642",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,md:myst",
   "text_representation": {
    "extension": ".md",
    "format_name": "myst",
    "format_version": 0.13,
    "jupytext_version": "1.13.3"
   }
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "source_map": [
   13,
   21,
   32,
   51,
   61,
   74,
   86,
   93,
   97,
   119,
   122,
   126,
   149,
   152,
   156,
   183,
   188,
   196,
   215,
   221,
   227,
   250,
   258,
   262,
   333,
   341,
   345
  ]
 },
 "nbformat": 4,
 "nbformat_minor": 5
}