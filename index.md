# OFC Labautomation Shortcourses 

Here you can find the instructions for the labautomation shortcourses at OFC2023. 

```{note} Click to view url
:class: dropdown
If you are seeing the presentation, the *URL* for the notes is:
[https://python4photonics.gitlab.io/ofcshortcourse/](https://python4photonics.gitlab.io/ofcshortcourse/)

or use this QR code:
![](./_static/QR.png)
```

You can find the notes for the two courses in the links below. If you'd like to
run the corresponding notebooks yourself you can get them from 
our [gitlab repository](https://gitlab.com/python4photonics/ofcshortcourse).
Generating the notebooks requires 
[jupytext](https://jupytext.readthedocs.io/en/latest/index.html). 
To generate all notebooks you can either open the `.md` files in jupyter notebook 
if you have the jupytext extension installed or use `make notebooks` in the root 
directory.


```{toctree}
beginner-book
advanced-book
```
