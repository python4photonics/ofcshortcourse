# Adopted from sphinx makefile
#

# You can set these variables from the command line, and also
# from the environment for the first two.
#

SPHINXOPTS    ?=
SPHINXBUILD   ?= sphinx-build
SOURCEDIR     = .
BUILDDIR      = _build
HTMLDIR = $(BUILDDIR)/html
PDFDIR = $(BUILDDIR)/pdf
NTB = ./allnotebooks
SUBDIR = Notebooks/Hands_on_Advanced Notebooks/Hands_on_Beginner
MDFILES:= $(shell find Notebooks -name \*.md | sed 's: :\\ :g' ) # need ot use find because of spaces in filenames
NOTEBOOKS = $(MDFILES:md=ipynb)




.PHONY: pdf clean-html html clean-beginner-html clean-advanced-html echoes

# Put it first so that "make" without argument is like "make all".
all:  html pdf

sphinx: 
	@$(SPHINXBUILD) -M html "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

beginner-html: clean-beginner-html
	jupyter-book build Notebooks/Hands_on_Beginner
	mkdir -p $(HTMLDIR)
	mv Notebooks/Hands_on_Beginner/_build/html $(HTMLDIR)/beginner-book

advanced-html: clean-advanced-html
	jupyter-book build Notebooks/Hands_on_Advanced
	mkdir -p $(HTMLDIR)
	mv Notebooks/Hands_on_Advanced/_build/html $(HTMLDIR)/advanced-book


html: sphinx beginner-html advanced-html beginner-html

%.ipynb: %.md 
	mkdir -p $(@D)
	jupytext --to ipynb -o "$@" "$<"

notebooks: $(NOTEBOOKS)

	
beginner-pdf: clean-beginner-pdf
	mkdir -p $(PDFDIR)
	jupyter-book build Notebooks/Hands_on_Beginner --builder pdflatex
	mv Notebooks/Hands_on_Beginner/_build/latex/python.pdf $(PDFDIR)/beginner-book.pdf
	
advanced-pdf: clean-advanced-pdf
	mkdir -p $(PDFDIR)
	jupyter-book build Notebooks/Hands_on_Advanced --builder pdflatex
	cp Notebooks/Hands_on_Advanced/_build/latex/python.pdf $(PDFDIR)/advanced-book.pdf

pdf: beginner-pdf  advanced-pdf

clean-notebooks: 
	rm -f $(NOTEBOOKS)

clean: clean-html clean-pdf
	rm -rf _build

clean-beginner-html:
	rm -rf _build/html/beginner-book

clean-advanced-html:
	rm -rf _build/html/advanced-book

clean-html: 
	rm -rf $(HTMLDIR)

clean-beginner-pdf:
	rm -f $(PDFDIR)/beginner-book.pdf

clean-advanced-pdf: 
	rm -f $(PDFDIR)/advanced-book.pdf

clean-pdf: clean-beginner-pdf clean-advanced-pdf
	rm -rf $(PDFDIR)

echoes:
	@echo "MD files: $(MDFILES)"
	@echo "Notebook files: $(NOTEBOOKS)"
